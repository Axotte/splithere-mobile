import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:splithere/application.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/blocs/friends_bloc.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/datasources/local/events_dao.dart';
import 'package:splithere/datasources/local/friends_dao.dart';
import 'package:splithere/datasources/local/profile_dao.dart';
import 'package:splithere/datasources/remote/client/api_client.dart';
import 'package:splithere/datasources/remote/events_api_provider.dart';
import 'package:splithere/datasources/remote/friends_api_provider.dart';
import 'package:splithere/datasources/remote/profile_api_provider.dart';
import 'package:splithere/repositories/events_repository.dart';
import 'package:splithere/repositories/friends_repository.dart';
import 'package:splithere/repositories/profile_repository.dart';

class AppModule extends ModuleWidget {
  @override
  List<Bloc> get blocs => [
    Bloc((i) => ProfileBloc(i.get())),
    Bloc((i) => EventsBloc(i.get())),
    Bloc((i) => FriendsBloc(i.get())),
  ];

  @override
  List<Dependency> get dependencies => [
    Dependency((_) => getApiClient()),
    Dependency((_) => FriendsDao()),
    Dependency((_) => ProfileDao()),
    Dependency((_) => EventsDao()),
    Dependency((i) => FriendsApiProvider(i.get())),
    Dependency((i) => ProfileApiProvider(i.get())),
    Dependency((i) => EventsApiProvider(i.get())),
    Dependency((i) => ProfileRepository(i.get(), i.get())),
    Dependency((i) => EventsRepository(i.get(), i.get())),
    Dependency((i) => FriendsRepository(i.get(), i.get())),
  ];

  @override
  Widget get view => Application();

  static Inject get injector => Inject<AppModule>.of();
}