import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:splithere/blocs/login_validation_bloc.dart';
import 'package:splithere/blocs/register_validation_bloc.dart';
import 'package:splithere/screens/login/login_screen.dart';

class LoginModule extends ModuleWidget {
  @override
  List<Bloc> get blocs => [
    Bloc((_) => RegisterValidationBloc()),
    Bloc((_) => LoginValidationBloc())
  ];

  @override
  List<Dependency> get dependencies => [];

  @override
  Widget get view => LoginScreen();

  static Inject get injector => Inject<LoginModule>.of();
}