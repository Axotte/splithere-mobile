import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:splithere/injection/login_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/screens/main/main_screen.dart';
import 'package:splithere/screens/splash/splash_screen.dart';

class Application extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      supportedLocales: _supportedLocales,
      localizationsDelegates: _localizationsDelegates,
      routes: _routes,
      title: 'splithere',
      theme: ThemeData(
          brightness: Brightness.light,
          // primary colors for application
          primaryColor: Color(0xFF1C2433),
          primaryColorLight: Color(0xFF2a3648),
          primaryColorDark: Color(0xFF171e2b),
          // main accent color
          accentColor: Color(0xFFDA3A47),
          // secondary accent color
          backgroundColor: Color(0xFF3a5bb9),
          // theme for all cards
          cardTheme: CardTheme(
            color: Color(0xFFf7f7f7),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
            margin: EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
          ),
          colorScheme: ColorScheme(
              brightness: Brightness.light,
              primary: Color(0xFF1C2433),
              primaryVariant: Color(0xFF171e2b),
              // for text displayed on primary color
              onPrimary: Color(0xFFF6F4F3),
              secondary: Colors.transparent,
              secondaryVariant: Colors.transparent,
              onSecondary: Colors.transparent,
              background: Colors.transparent,
              onBackground: Colors.transparent,
              // surface describes colors used in EventDetails tabbar
              surface: Color(0xFFf7f7f7),
              // onSurface is the color of unselected tab
              onSurface: Color(0xFF747372),
              // color for tabbarview background in EventDetails
              error: Color(0xFFe5e5e5),
              onError: Color(0xFFDA3A47)
          ),
          // divider color
          dividerColor: Color(0xFF747372),
          textTheme: TextTheme(
            // used for cards font color
            bodyText1: TextStyle(color: Color(0xFF1A1A1A)),
            // used for buttons font color
            bodyText2: TextStyle(color: Color(0xFFFCFCFC)),
          )
      ),
      darkTheme: ThemeData(
          brightness: Brightness.dark,
          // primary colors for application
          primaryColor: Color(0xFF2b2c2f),
          primaryColorLight: Color(0xFF424347),
          primaryColorDark: Color(0xFF29292c),
          // main accent color
          accentColor: Color(0xFFDA3A47),
          // secondary accent color
          backgroundColor: Color(0xFF3a5bb9),
          cardTheme: CardTheme(
            color: Color(0xFF424347),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
            margin: EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
          ),
          colorScheme: ColorScheme(
              brightness: Brightness.dark,
              primary: Color(0xFF2b2c2f),
              primaryVariant: Color(0xFF29292c),
              // for text displayed on primary color
              onPrimary: Color(0xFFF6F4F3),
              secondary: Colors.transparent,
              secondaryVariant: Colors.transparent,
              onSecondary: Colors.transparent,
              background: Colors.transparent,
              onBackground: Colors.transparent,
              // surface describes colors used in EventDetails tabbar
              surface: Color(0xFF212123),
              // onSurface is the color of unselected tab
              onSurface: Color(0xFFc1c1c1),
              error: Color(0xFF262629),
              onError: Color(0xFFDA3A47)
          ),
          dividerColor: Color(0xFF747372),
          textTheme: TextTheme(
            // used for cards font color
            bodyText1: TextStyle(color: Color(0xFFF6F4F3)),
            // used for buttons font color
            bodyText2: TextStyle(color: Color(0xFFFCFCFC)),
          )
      ),
    );
  }

  List<Locale> get _supportedLocales => [
    Locale('en'),
    Locale('pl'),
  ];

  List<LocalizationsDelegate> get _localizationsDelegates => [
    Lang.delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  Map<String, Widget Function(BuildContext)> get _routes => {
    '/': (context) => SplashScreen(),
    '/login': (context) => LoginModule(),
    '/main': (context) => MainScreen()
  };
}