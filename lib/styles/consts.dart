class Consts {
  static const double dialogPadding = 18.0;
  static const double dialogLogoRadius = 36.0;

  static const double avatarPadding = 24.0;
  static const double avatarRadius = 64.0;
  static const double avatarEditRadius = 20.0;

  static const double eventPadding = 22.0;
  static const double eventLogoRadius = 44.0;
}