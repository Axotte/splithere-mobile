import 'package:flutter/widgets.dart';

class SplithereIcons {
  SplithereIcons._();

  static const _kFontFam = 'SplithereIcons';
  static const _kFontPkg = null;

  static const IconData logo = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
