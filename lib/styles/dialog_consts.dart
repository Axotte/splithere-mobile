import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:splithere/styles/consts.dart';

class DialogConsts {
  static const List<BoxShadow> DIALOG_SHADOW = [
    BoxShadow(
      color: Colors.black26,
      blurRadius: 10.0,
      offset: const Offset(0.0, 10.0),
    )
  ];

  static const EdgeInsets DIALOG_PADDING_AVATAR = EdgeInsets.only(
    top: Consts.avatarRadius + Consts.avatarPadding * 0.5,
    bottom: Consts.avatarPadding,
    left: Consts.avatarPadding,
    right: Consts.avatarPadding,
  );

  static const EdgeInsets DIALOG_PADDING_ELSE = EdgeInsets.only(
    top: Consts.dialogLogoRadius + Consts.dialogPadding * 0.5,
    bottom: Consts.dialogPadding,
    left: Consts.dialogPadding,
    right: Consts.dialogPadding,
  );
}