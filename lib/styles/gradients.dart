import 'package:flutter/material.dart';

class Gradients {
  static const Color _LIGHT_DARKSHADE = Color.fromARGB(1, 228, 228, 228);
  static const Color _LIGHT_MEDIUMHADE = Color.fromARGB(1, 242, 242, 242);
  static const Color _LIGHT_LIGHTSHADE = Color.fromARGB(1, 249, 249, 249);

  static const LIGHT_CARD_GRADIENT = LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      stops: [0.05, 0.15, 1],
      colors: [
        _LIGHT_DARKSHADE,
        _LIGHT_MEDIUMHADE,
        _LIGHT_LIGHTSHADE
      ]
    );

  static const Color _DARK_DARKSHADE = Color.fromARGB(1, 228, 228, 228);
  static const Color _DARK_MEDIUMHADE = Color.fromARGB(1, 242, 242, 242);
  static const Color _DARK_LIGHTSHADE = Color.fromARGB(1, 249, 249, 249);

  static const DARK_CARD_GRADIENT = LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      stops: [0.05, 0.15, 1],
      colors: [
        _DARK_DARKSHADE,
        _DARK_MEDIUMHADE,
        _DARK_LIGHTSHADE
      ]
  );
}