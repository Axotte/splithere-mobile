import 'package:json_annotation/json_annotation.dart';
import 'package:splithere/config/app_config.dart';
import 'package:splithere/enums/grant_type.dart';
import 'package:splithere/models/token/token.dart';

part 'token_request.g.dart';

@JsonSerializable()
class TokenRequest {
  @JsonKey(name: 'grant_type', fromJson: grantTypeFromString, toJson: getStringFromGrantType, includeIfNull: false)
  GrantType grantType;
  @JsonKey(name: 'client_id', includeIfNull: false)
  String clientId;
  @JsonKey(name: 'client_secret', includeIfNull: false)
  String clientSecret;
  @JsonKey(name: 'username', includeIfNull: false)
  String username;
  @JsonKey(name: 'password', includeIfNull: false)
  String password;
  @JsonKey(name: 'confirm_password', includeIfNull: false)
  String confirmPassword;
  @JsonKey(name: 'refresh_token', includeIfNull: false)
  String refreshToken;
  @JsonKey(name: 'email', includeIfNull: false)
  String email;
  @JsonKey(name: 'nickname', includeIfNull: false)
  String nickname;

  TokenRequest({
    this.refreshToken,
    this.confirmPassword,
    this.grantType,
    this.password,
    this.username,
    this.email,
    this.nickname
  }) {
    clientSecret = AppConfig.clientSecret;
    clientId = AppConfig.clientId;
  }

  factory TokenRequest.fromToken(Token token) => TokenRequest(
    grantType: GrantType.REFRESH_TOKEN,
    refreshToken: token.refreshToken
  );

  factory TokenRequest.fromLogin(String email, String password) => TokenRequest(
    grantType: GrantType.PASSWORD,
    username: email,
    password: password
  );

  factory TokenRequest.fromRegister(String email, String login, String password, String confirmPassword) => TokenRequest(
    grantType: GrantType.PASSWORD,
    password: password,
    username: email,
    confirmPassword: confirmPassword,
    email: email,
    nickname: login
  );

  factory TokenRequest.fromJson(Map<String, dynamic> json) => _$TokenRequestFromJson(json);
  Map<String, dynamic> toJson() => _$TokenRequestToJson(this);
}