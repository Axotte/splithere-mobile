import 'package:json_annotation/json_annotation.dart';

part 'token.g.dart';

int getExpireDate(int seconds) {
  if (seconds > 100000) return seconds;
  return DateTime.now().add(Duration(seconds: seconds - 10)).millisecondsSinceEpoch;
}

@JsonSerializable()
class Token {
  @JsonKey(name: 'access_token')
  String accessToken;
  @JsonKey(name: 'token_type')
  String tokenType;
  @JsonKey(name: 'scope')
  String scope;
  @JsonKey(name: 'refresh_token')
  String refreshToken;
  @JsonKey(name: 'expires_in', fromJson: getExpireDate)
  int expireDateInMillis;

  Token({
    this.expireDateInMillis,
    this.accessToken,
    this.refreshToken,
    this.scope,
    this.tokenType
  });

  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);
  Map<String, dynamic> toJson() => _$TokenToJson(this);
}