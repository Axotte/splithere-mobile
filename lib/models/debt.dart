import 'package:json_annotation/json_annotation.dart';
import 'package:splithere/models/user/member.dart';

part 'debt.g.dart';

@JsonSerializable(explicitToJson: true)
class Debt {
  int id;
  String event;
  Member debtor;
  Member creditor;
  double debt;
  int status;

  Debt({
    this.id,
    this.event,
    this.status,
    this.creditor,
    this.debt,
    this.debtor
  });

  factory Debt.fromJson(Map<String, dynamic> json) => _$DebtFromJson(json);

  Map<String, dynamic> toJson() => _$DebtToJson(this);
}