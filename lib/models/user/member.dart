import 'package:json_annotation/json_annotation.dart';
import 'package:splithere/models/user/user.dart';

part 'member.g.dart';

@JsonSerializable(explicitToJson: true)
class Member {
  @JsonKey(includeIfNull: false)
  User user;
  @JsonKey(name: 'phone_number', includeIfNull: false)
  String phoneNumber;
  @JsonKey(name: 'account_number', includeIfNull: false)
  String accountNumber;
  @JsonKey(includeIfNull: false)
  String avatar;

  Member({
    this.user,
    this.avatar,
    this.phoneNumber,
    this.accountNumber
  });

  Member toAdd() => Member(user: User(username: this.user.username));

  factory Member.fromJson(Map<String, dynamic> json) => _$MemberFromJson(json);

  Map<String, dynamic> toJson() => _$MemberToJson(this);
}