import 'package:json_annotation/json_annotation.dart';
import 'package:splithere/models/user/member.dart';

part 'event.g.dart';

@JsonSerializable(explicitToJson: true)
class Event {
  @JsonKey(includeIfNull: false)
  int id;
  @JsonKey(includeIfNull: false)
  String name;
  @JsonKey(includeIfNull: false)
  Member host;
  @JsonKey(includeIfNull: false)
  int status;
  @JsonKey(name: 'member_set', includeIfNull: false)
  List<Member> members;
  @JsonKey(includeIfNull: false)
  String lat;
  @JsonKey(includeIfNull: false)
  String lon;
  @JsonKey(includeIfNull: false)
  String picture;

  Event({
    this.name,
    this.id,
    this.host,
    this.members,
    this.status,
    this.picture,
    this.lat,
    this.lon
  });

  factory Event.fromJson(Map<String, dynamic> json) => _$EventFromJson(json);

  Map<String, dynamic> toJson() => _$EventToJson(this);
}