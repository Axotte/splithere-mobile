import 'package:splithere/models/user/member.dart';
import 'package:json_annotation/json_annotation.dart';

part 'bill.g.dart';

@JsonSerializable(explicitToJson: true)
class Bill {
  String event;
  int id;
  double expense;
  Member payer;
  DateTime date;
  String photo;

  Bill({
    this.event,
    this.id,
    this.expense,
    this.payer,
    this.photo,
    this.date
  });

  factory Bill.fromJson(Map<String, dynamic> json) => _$BillFromJson(json);

  Map<String, dynamic> toJson() => _$BillToJson(this);
}