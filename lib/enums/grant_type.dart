import 'package:flutter/foundation.dart';

enum GrantType {
  PASSWORD, REFRESH_TOKEN
}

String getStringFromGrantType(GrantType type) => describeEnum(type).toLowerCase();

GrantType grantTypeFromString(String value) {
  switch(value) {
    case 'password':
      return GrantType.PASSWORD;
    case 'refresh_token':
      return GrantType.REFRESH_TOKEN;
    default:
      return null;
  }
}