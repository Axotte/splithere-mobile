import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:splithere/datasources/remote/client/api_client.dart';
import 'package:splithere/models/bill.dart';
import 'package:splithere/models/debt.dart';
import 'package:splithere/models/token/token_request.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/repositories/profile_repository.dart';

class ProfileBloc extends BlocBase {
  ProfileRepository _profileRepository;
  StreamSubscription _logoutSubscription;

  ProfileBloc(this._profileRepository) {
    _logoutSubscription = logoutStream.listen((_) {
      _currentUserSubject.add(null);
      _myDebtsSubject.add(null);
      _myLoansSubject.add(null);
      _myBillsSubject.add(null);
      _profileRepository.clearDatabase();
    });

    deleteBillStream = _deleteBillSubject
        .throttleTime(const Duration(seconds: 1), trailing: true)
        .switchMap((bill) => _profileRepository.deleteBill(bill.id).asStream())
        .doOnData(refreshAllMyBills)
        .asBroadcastStream();

    updateDebtStream = _updateDebtSubject
        .throttleTime(const Duration(seconds: 1), trailing: true)
        .switchMap((debt) => _profileRepository.updateDebtStatus(debt.id, debt).asStream())
        .doOnData(refreshAllMyDebts)
        .asBroadcastStream();

    updateLoanStream = _updateLoanSubject
        .throttleTime(const Duration(seconds: 1), trailing: true)
        .switchMap((loan) => _profileRepository.updateLoanStatus(loan.id, loan).asStream())
        .doOnData(refreshAllMyDebts)
        .asBroadcastStream();

    updateUserStream = _updateUserSubject
        .doOnData(startLoading)
        .switchMap((member) => _profileRepository.updateUser(member).asStream())
        .doOnData(refreshCurrentUser)
        .doOnEach(stopLoading)
        .asBroadcastStream();
  }

  BehaviorSubject<Member> _currentUserSubject = BehaviorSubject();
  Stream<Member> get currentUserStream => _currentUserSubject.stream;

  BehaviorSubject<List<Bill>> _myBillsSubject = BehaviorSubject();
  Stream<List<Bill>> get myBillsStream => _myBillsSubject.stream;

  BehaviorSubject<List<Debt>> _myDebtsSubject = BehaviorSubject();
  Stream<List<Debt>> get myDebtsStream => _myDebtsSubject.stream;

  BehaviorSubject<List<Debt>> _myLoansSubject = BehaviorSubject();
  Stream<List<Debt>> get myLoansStream => _myLoansSubject.stream;

  PublishSubject<Bill> _deleteBillSubject = PublishSubject();
  Stream deleteBillStream;
  void Function(Bill) get deleteBill => _deleteBillSubject.add;

  PublishSubject<Debt> _updateDebtSubject = PublishSubject();
  Stream updateDebtStream;
  void Function(Debt) get updateDebt => _updateDebtSubject.add;

  PublishSubject<Debt> _updateLoanSubject = PublishSubject();
  Stream updateLoanStream;
  void Function(Debt) get updateLoan => _updateLoanSubject.add;

  PublishSubject<Member> _updateUserSubject = PublishSubject();
  Stream updateUserStream;
  void Function(Member) get updateUser => _updateUserSubject.add;

  PublishSubject<bool> _loadingSubject = PublishSubject();
  Stream get loadingStream => _loadingSubject.stream;
  
  void startLoading([_]) => _loadingSubject.add(true);

  void stopLoading([_]) => _loadingSubject.add(false);


  refreshCurrentUser([_]) {
    _profileRepository.getCurrentUser()
        .then(_currentUserSubject.add)
        .catchError(_currentUserSubject.addError);
  }

  login(TokenRequest tokenRequest) {
    startLoading();
    _profileRepository.login(tokenRequest)
        .then(_currentUserSubject.add)
        .then(stopLoading)
        .catchError(_currentUserSubject.addError)
        .then(stopLoading);
  }

  register(TokenRequest tokenRequest) {
    startLoading();
    _profileRepository.register(tokenRequest)
        .then(_currentUserSubject.add)
        .then(stopLoading)
        .catchError(_currentUserSubject.addError)
        .then(stopLoading);

  }

  refreshAllMyBills([_]) => _profileRepository.getAllMyBills()
      .then(_myBillsSubject.add)
      .catchError(_myBillsSubject.addError);

  refreshAllMyLoans([_]) => _profileRepository.getAllMyLoans()
      .then(_myLoansSubject.add)
      .catchError(_myLoansSubject.addError);

  refreshAllMyDebts([_]) => _profileRepository.getAllMyDebts()
      .then(_myDebtsSubject.add)
      .catchError(_myDebtsSubject.addError);

  Member get currentUser => _currentUserSubject.value;

  @override
  void dispose() {
    super.dispose();
    _logoutSubscription.cancel();
    _currentUserSubject.close();
    _myBillsSubject.close();
    _myDebtsSubject.close();
    _myLoansSubject.close();
    _updateDebtSubject.close();
    _updateLoanSubject.close();
    _deleteBillSubject.close();
    _updateUserSubject.close();
    _loadingSubject.close();
  }
}