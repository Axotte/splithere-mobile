import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:splithere/datasources/remote/client/api_client.dart';
import 'package:splithere/models/bill.dart';
import 'package:splithere/models/debt.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/repositories/events_repository.dart';

class EventsBloc extends BlocBase {
  EventsRepository _eventsRepository;
  StreamSubscription _logoutSubscription;

  EventsBloc(this._eventsRepository) {
    _logoutSubscription = logoutStream.listen((_) {
      _eventsSubject.add(null);
      _eventsRepository.clearDatabase();
    });

    addEventStream = _addEventSubject
        .doOnData(startLoading)
        .switchMap((event) => _eventsRepository.createEvent(event).asStream())
        .doOnData(updateEvents)
        .doOnEach(stopLoading)
        .asBroadcastStream();
    editEventStream = _editEventSubject
        .doOnData(startLoading)
        .switchMap((event) => _eventsRepository.updateEvent(event).asStream())
        .doOnData(updateEvents)
        .doOnEach(stopLoading)
        .asBroadcastStream();
    deleteEventStream = _removeEventSubject
        .throttleTime(const Duration(seconds: 1), trailing: true)
        .switchMap((id) => _eventsRepository.deleteEvent(id).asStream())
        .doOnData(updateEvents)
        .asBroadcastStream();
  }

  BehaviorSubject<List<Event>> _eventsSubject = BehaviorSubject();
  Stream<List<Event>> get eventsObservable => _eventsSubject.stream;
  Stream<Event> getEventObservable(int id) => eventsObservable
      .map((event) => event.firstWhere((element) => element.id == id, orElse: () => null))
      .asBroadcastStream();

  BehaviorSubject<List<Bill>> _eventBillsSubject = BehaviorSubject();
  Stream<List<Bill>> get eventBillsStream => _eventBillsSubject.stream;
  Stream<double> get totalCostsStream => eventBillsStream.map((bills) {
    if(bills.isEmpty) return 0.0;
    return bills.map((bill) => bill.expense)
        .reduce((value, element) => value + element);
  });

  BehaviorSubject<List<Debt>> _eventDebtsSubject = BehaviorSubject();
  Stream<List<Debt>> get eventDebtsStream => _eventDebtsSubject.stream;

  BehaviorSubject<List<Debt>> _eventLoansSubject = BehaviorSubject();
  Stream<List<Debt>> get eventLoansStream => _eventLoansSubject.stream;

  PublishSubject _addBillSubject = PublishSubject();
  Stream get addBillStream => _addBillSubject.stream;

  PublishSubject<Event> _addEventSubject = PublishSubject();
  void Function(Event) get addEvent => _addEventSubject.add;
  Stream addEventStream;

  PublishSubject<Event> _editEventSubject = PublishSubject();
  void Function(Event) get editEvent => _editEventSubject.add;
  Stream editEventStream;

  PublishSubject<int> _removeEventSubject = PublishSubject();
  void Function(int) get removeEvent => _removeEventSubject.add;
  Stream deleteEventStream;

  PublishSubject<bool> _loadingSubject = PublishSubject();
  Stream get loadingStream => _loadingSubject.stream;

  void startLoading([_]) => _loadingSubject.add(true);

  void stopLoading([_]) => _loadingSubject.add(false);

  updateEvents([_]) => _eventsRepository.getAllEvents()
      .then(_eventsSubject.add)
      .catchError(_eventsSubject.addError);

  getEventBills(int eventId) => _eventsRepository.getEventBills(eventId)
      .then(_eventBillsSubject.add)
      .catchError(_eventBillsSubject.addError);

  getEventDebts(int eventId) => _eventsRepository.getEventDebts(eventId)
      .then(_eventDebtsSubject.add)
      .catchError(_eventDebtsSubject.addError);

  getEventLoans(int eventId) => _eventsRepository.getEventLoans(eventId)
      .then(_eventLoansSubject.add)
      .catchError(_eventLoansSubject.addError);

  Future addBill(eventId, Bill bill) {
    startLoading();
    return _eventsRepository.addBill(eventId, bill)
        .then(_addBillSubject.add)
        .then(stopLoading)
        .catchError(_addBillSubject.addError)
        .then(stopLoading);
  }

  clearSubjects() {
    _eventBillsSubject.add(null);
    _eventLoansSubject.add(null);
    _eventDebtsSubject.add(null);
  }

  @override
  void dispose() {
    super.dispose();
    _logoutSubscription.cancel();
    _eventsSubject.close();
    _addEventSubject.close();
    _editEventSubject.close();
    _removeEventSubject.close();
    _eventBillsSubject.close();
    _eventDebtsSubject.close();
    _eventLoansSubject.close();
    _addBillSubject.close();
  }
}