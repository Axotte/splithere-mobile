import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:splithere/models/token/token_request.dart';

class RegisterValidationBloc extends BlocBase {
  BehaviorSubject<String> _loginSubject = BehaviorSubject();
  Stream<String> get loginStream => _loginSubject.stream;
  Function get changeLogin => _loginSubject.add;

  BehaviorSubject<String> _emailSubject = BehaviorSubject();
  Stream<String> get emailStream => _emailSubject.stream;
  Function get changeEmail => _emailSubject.add;

  BehaviorSubject<String> _passwordSubject = BehaviorSubject();
  Stream<String> get passwordStream => _passwordSubject.stream;
  Function get changePassword => _passwordSubject.add;

  BehaviorSubject<String> _repeatPasswordSubject = BehaviorSubject();
  Stream<String> get repeatPasswordStream => _repeatPasswordSubject.stream;
  Function get changeRepeatPassword => _repeatPasswordSubject.add;

  TokenRequest validateAndGetRequest() {
    String login = _loginSubject.value;
    String email = _emailSubject.value;
    String password = _passwordSubject.value;
    String repeatPassword = _repeatPasswordSubject.value;

    bool valid = true;

    if ((login ?? '').isEmpty) {
      valid = false;
      _loginSubject.addError('empty_field_error');
    }
    if ((email ?? '').isEmpty) {
      valid = false;
      _emailSubject.addError('empty_field_error');
    }
    if ((password ?? '').isEmpty) {
      valid = false;
      _passwordSubject.addError('empty_field_error');
    }
    if ((repeatPassword ?? '').isEmpty) {
      valid = false;
      _repeatPasswordSubject.addError('empty_field_error');
    }

    if (!valid) return null;

    if (password.length < 6) {
      valid = false;
      _passwordSubject.addError('password_to_short_error');
      _repeatPasswordSubject.addError('password_to_short_error');
    }

    if (password != repeatPassword) {
      valid = false;
      _passwordSubject.addError('different_password_error');
      _repeatPasswordSubject.addError('different_password_error');
    }

    if (!valid) return null;

    return TokenRequest.fromRegister(email, login, password, repeatPassword);
  }



  @override
  void dispose() {
    super.dispose();
    _loginSubject.close();
    _emailSubject.close();
    _passwordSubject.close();
    _repeatPasswordSubject.close();
  }
}