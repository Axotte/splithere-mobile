import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:splithere/datasources/remote/client/api_client.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/models/user/user.dart';
import 'package:splithere/repositories/friends_repository.dart';

class FriendsBloc extends BlocBase {
  FriendsRepository _friendsRepository;
  StreamSubscription _logoutSubscription;

  FriendsBloc(this._friendsRepository) {
    _logoutSubscription = logoutStream.listen((_) {
      _friendsSubject.add(null);
      _friendsRepository.clearDatabase();
    });

    addFriendStream = _addFriendSubject
        .doOnData(startLoading)
        .switchMap((friend) => _friendsRepository.addFriend(friend).asStream())
        .doOnData(updateFriends)
        .doOnEach(stopLoading)
        .asBroadcastStream();
    removeFriendStream = _removeFriendSubject
        .throttleTime(const Duration(seconds: 1), trailing: true)
        .switchMap((friend) => _friendsRepository.removeFriend(friend).asStream())
        .doOnData(updateFriends)
        .asBroadcastStream();
  }

  BehaviorSubject<List<Member>> _friendsSubject = BehaviorSubject();
  Stream<List<Member>> get friendsStream => _friendsSubject.stream;

  PublishSubject<User> _addFriendSubject = PublishSubject();
  void Function(User) get addFriend => _addFriendSubject.add;
  Stream addFriendStream;

  PublishSubject<User> _removeFriendSubject = PublishSubject();
  void Function(User) get removeFriend => _removeFriendSubject.add;
  Stream removeFriendStream;

  PublishSubject<bool> _loadingSubject = PublishSubject();
  Stream get loadingStream => _loadingSubject.stream;

  void startLoading([_]) => _loadingSubject.add(true);

  void stopLoading([_]) => _loadingSubject.add(false);

  updateFriends([_]) {
    _friendsRepository.getAllFriends()
        .then(_friendsSubject.add)
        .catchError(_friendsSubject.addError);
  }

  @override
  void dispose() {
    super.dispose();
    _friendsSubject.close();
    _addFriendSubject.close();
    _removeFriendSubject.close();
    _logoutSubscription.cancel();
  }
}