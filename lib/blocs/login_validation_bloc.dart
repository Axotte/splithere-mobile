import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:splithere/models/token/token_request.dart';

class LoginValidationBloc extends BlocBase {

  BehaviorSubject<String> _emailSubject = BehaviorSubject();
  Stream<String> get emailStream => _emailSubject.stream;
  Function get changeEmail => _emailSubject.add;

  BehaviorSubject<String> _passwordSubject = BehaviorSubject();
  Stream<String> get passwordStream => _passwordSubject.stream;
  Function get changePassword => _passwordSubject.add;

  TokenRequest validateAndGetRequest() {
    String email = _emailSubject.value;
    String password = _passwordSubject.value;

    bool valid = true;

    if ((email ?? '').isEmpty) {
      valid = false;
      _emailSubject.addError('empty_field_error');
    }
    if ((password ?? '').isEmpty) {
      valid = false;
      _passwordSubject.addError('empty_field_error');
    }
    if (!valid) return null;

    return TokenRequest.fromLogin(email, password);
  }



  @override
  void dispose() {
    super.dispose();
    _emailSubject.close();
    _passwordSubject.close();
  }
}