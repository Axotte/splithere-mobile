import 'package:flutter/material.dart';
import 'package:splithere/utils/app_colors.dart';

class DoubleRow extends StatelessWidget {
  final String label1;
  final String label2;
  final String text1;
  final String text2;
  final Color labelColor;
  final Color textColor;
  final double fontSize;
  final double widthFactor;
  final double lowerFont;
  final CrossAxisAlignment crossAxisAlignment;
  final bool hasOptions;
  final void Function() onPressed;

  DoubleRow({this.label1, this.label2, this.text1, this.text2, this.lowerFont: 1.0,
    this.crossAxisAlignment = CrossAxisAlignment.center, this.labelColor,
    this.textColor, this.fontSize, this.widthFactor: 0.8, this.onPressed, this.hasOptions = false});

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: widthFactor,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: crossAxisAlignment,
              children: <Widget>[
                Text(label1, style: TextStyle(fontSize: fontSize - lowerFont,
                    fontWeight: FontWeight.w500, color: labelColor)),
                SizedBox(height: 2.0),
                Text(text1, style: TextStyle(fontSize: fontSize,
                    color: textColor, fontWeight: FontWeight.w500)),
              ],
            ),
          ),
          Expanded(flex: 0, child: Container(width: 12.0,)),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: crossAxisAlignment,
              children: <Widget>[
                Text(label2, style: TextStyle(fontSize: fontSize - lowerFont,
                    fontWeight: FontWeight.w500, color: labelColor)),
                SizedBox(height: 2.0),
                Text(text2, style: TextStyle(fontSize: fontSize,
                    color: textColor, fontWeight: FontWeight.w500)),
              ],
            ),
          ),
          hasOptions == false ? Container():
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.only(left: 12.0),
              child: IconButton(
                icon: Icon(Icons.settings),
                onPressed: onPressed,
                color: Theme.of(context).colorScheme.onPrimary,
                iconSize: 26.0,
              ),
            ),
          )
        ],
      ),
    );
  }
}
