import 'package:flutter/material.dart';
import 'package:splithere/localizations/app_localizations.dart';


class InputTextField extends StatefulWidget {
  final String hint;
  final String label;
  final bool obscure;
  final IconData icon;
  final TextEditingController controller;
  final FocusNode focusNode;
  final void Function(String) onFieldSubmitted;
  final void Function(String) onChange;
  final Stream<String> errorStream;

  InputTextField({
    this.icon,
    this.hint,
    this.label,
    this.obscure,
    this.controller,
    this.focusNode,
    this.onFieldSubmitted,
    this.errorStream,
    this.onChange
  });


  @override
  State<StatefulWidget> createState() => InputTextFieldState();

}


class InputTextFieldState extends State<InputTextField> with SingleTickerProviderStateMixin{

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
      stream: widget.errorStream,
      builder: (context, snapshot) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
          child: AnimatedSize(
            vsync: this,
            duration: const Duration(milliseconds: 250),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: snapshot.hasError
                          ? Theme.of(context).colorScheme.onError.withOpacity(0.4)
                          : Theme.of(context).colorScheme.onPrimary.withOpacity(0.4),
                      width: 1
                    ),
                    borderRadius: BorderRadius.circular(14.0),
                  ),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
                        child: Icon(widget.icon, color: Theme.of(context).colorScheme.onPrimary, size: 24.0,),
                      ),
                      Container(
                        height: 26.0, width: 1.0,
                        color: Colors.grey[400].withOpacity(0.9),
                        margin: const EdgeInsets.only(right: 16.0),
                      ),
                      Expanded(
                        child: Container(
                          margin: const EdgeInsets.only(right: 16.0),
                          child: TextFormField(
                            obscureText: widget.obscure,
                            focusNode: widget.focusNode,
                            controller: widget.controller,
                            onFieldSubmitted: widget.onFieldSubmitted,
                            onChanged: widget.onChange,
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.onPrimary,
                              fontSize: 16.0
                            ),
                            decoration: InputDecoration(
                              isDense: true,
                              labelText: widget.label,
                              labelStyle: TextStyle(color: Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
                              border: InputBorder.none,
                              hintText: widget.hint,
                              hintStyle: TextStyle(color: Theme.of(context).colorScheme.onPrimary.withOpacity(0.5)),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                if (snapshot.hasError) Padding(
                  padding: const EdgeInsets.only(top: 8, left: 8),
                  child: Text(Lang.of(context).translate(snapshot.error),
                    style: TextStyle(
                      fontSize: 10,
                      color: Theme.of(context).colorScheme.onError
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      }
    );
  }
}
