import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:splithere/utils/animation_info.dart';

class FadeAnimation extends StatelessWidget {

  final double delay;
  final Widget child;
  final bool isVisible;

  FadeAnimation({this.delay, this.child, this.isVisible});

  @override
  Widget build(BuildContext context) {

    final tween = isVisible ? MultiTrackTween([
      Track("opacity").add(AppAnimationInfo.TWEEN_ANIMATION_DURATION, Tween(begin: 0.0, end: 1.0)),
      Track("translateX").add(
          Duration(milliseconds: 500),
          Tween(begin: -240.0, end: 0.0),
          curve: Curves.easeOut)
    ]) : MultiTrackTween([
      Track("opacity").add(AppAnimationInfo.TWEEN_ANIMATION_DURATION, Tween(begin: 1.0, end: 0.0)),
      Track("translateX").add(
          Duration(milliseconds: 500),
          Tween(begin: 0.0, end: 720.0),
          curve: Curves.easeOut)
    ]);

    return ControlledAnimation(
      delay: Duration(milliseconds: (500 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builderWithChild: (context, child, animation) => Opacity(
        opacity: animation["opacity"],
        child: Transform.translate(
          offset: Offset(animation["translateX"], 0),
          child: child,
        ),
      ),
    );
  }
}