import 'package:flutter/material.dart';

class SingleRow extends StatelessWidget {
  final String label;
  final String text;
  final Color labelColor;
  final Color textColor;
  final double fontSize;
  final double lowerFont;
  final double widthFactor;
  final CrossAxisAlignment crossAxisAlignment;
  final bool labelFirst;

  SingleRow({this.label, this.text, this.fontSize = 22.0, this.textColor, this.labelColor,
    this.crossAxisAlignment: CrossAxisAlignment.start, this.labelFirst: true,
    this.lowerFont: 8.0, this.widthFactor: 0.9});

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: widthFactor,
      child: labelFirst ? Column(
          crossAxisAlignment: crossAxisAlignment,
          children: <Widget>[
            Text(label, style: TextStyle(fontSize: fontSize-lowerFont,
                fontWeight: FontWeight.w500, color: labelColor)),
            Text(text, style: TextStyle(fontSize: fontSize, color: textColor,
                fontWeight: FontWeight.w500)),
          ]
      ) : Column(
          crossAxisAlignment: crossAxisAlignment,
          children: <Widget>[
            Text(text, style: TextStyle(fontSize: fontSize, color: textColor,
              fontWeight: FontWeight.w500)),
            Text(label, style: TextStyle(fontSize: fontSize-lowerFont,
                fontWeight: FontWeight.w500, color: labelColor)),
          ]
      )
    );
  }
}
