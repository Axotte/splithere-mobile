import 'package:flutter/material.dart';

typedef void OnClick();

class FormFieldButton extends StatelessWidget {
  final String text;
  final Color mainColor;
  final Color textColor;
  final IconData icon;
  final OnClick onPressed;
  final Color splashColor;

  FormFieldButton({this.text, this.mainColor, this.textColor,
    this.icon, this.onPressed, this.splashColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: FlatButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
              padding: EdgeInsets.symmetric(vertical: 9.0),
              splashColor: splashColor,
              color: mainColor,
              child: new Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 16.0),
                    child: Text(text.toUpperCase(),
                      style: TextStyle(color: textColor, fontSize: 15.0),
                    ),
                  ),
                  Expanded(child: Container()),
                  Transform.translate(
                    offset: Offset(-10.0, 0.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: textColor,
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 24.0),
                      child: Icon(icon, color: mainColor,),
                    ),
                  )
                ],
              ),
              onPressed: onPressed,
            ),
          ),
        ],
      ),
    );
  }
}
