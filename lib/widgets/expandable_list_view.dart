import 'package:flutter/material.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/debt.dart';
import 'package:splithere/screens/main/widgets/debt_loan_card.dart';
import 'package:splithere/widgets/expandable_container.dart';

class ExpandableListView extends StatefulWidget {

  final String titleKey;
  final String cardStringKey;
  final double expandedHeight;
  final bool expanded;
  final void Function() onTap;
  final List<Debt> items;

  const ExpandableListView({this.titleKey, this.cardStringKey,
    this.expandedHeight, this.expanded, this.onTap, this.items});

  @override
  _ExpandableListViewState createState() => new _ExpandableListViewState();
}

class _ExpandableListViewState extends State<ExpandableListView> {

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          color: Theme.of(context).accentColor,
          width: MediaQuery.of(context).size.width,
          height: 50.0,
          child: FractionallySizedBox(
            widthFactor: 0.96,
            child: InkWell(
              onTap: widget.onTap,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(Lang.of(context).translate(widget.titleKey),
                    style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18.0,
                    color: Theme.of(context).textTheme.bodyText2.color),
                  ),
                  Icon(
                    widget.expanded ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
                    color: Theme.of(context).textTheme.bodyText2.color,
                    size: 30.0,
                  ),
                ],
              ),
            ),
          ),
        ),
        ExpandableContainer(
          expanded: widget.expanded,
          expandedHeight: widget.expandedHeight,
          child: ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return DebtLoanCard(stringKey: widget.cardStringKey, item: widget.items[index],);
            },
            itemCount: widget.items.length,
          )
        )
      ],
    );
  }
}