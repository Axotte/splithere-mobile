import 'package:flutter/material.dart';

class DialogPopup extends StatefulWidget {
  final Widget child;

  DialogPopup({this.child});

  @override
  State<StatefulWidget> createState() => DialogPopupState();
}

class DialogPopupState extends State<DialogPopup>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.bounceInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: MediaQuery.of(context).viewInsets,
        child: SingleChildScrollView(
          child: Material(
            color: Colors.transparent,
            child: ScaleTransition(
                scale: scaleAnimation,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 12.0),
                  child: widget.child,
                )
            ),
          ),
        ),
      ),
    );
  }
}