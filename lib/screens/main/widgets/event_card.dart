import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/screens/main/views/confirmation_dialog.dart';
import 'package:splithere/screens/main/views/event_details.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/custom_popupmenu_item.dart';

class EventListItem extends StatefulWidget {
  final Event event;
  EventListItem(this.event);

  @override
  _EventListItemState createState() => _EventListItemState();
}

class _EventListItemState extends State<EventListItem> {
  var _tapPosition;

  EventsBloc _eventBloc;
  ProfileBloc _profileBloc;
  StreamSubscription _deleteEventStream;

  @override
  void initState() {
    super.initState();

    _eventBloc = AppModule.injector.getBloc();
    _profileBloc = AppModule.injector.getBloc();

    _deleteEventStream = _eventBloc.deleteEventStream.listen((event) {
      showToast(context, Lang.of(context).translate('deleted'));
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _deleteEventStream.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        child: GestureDetector(
          onTap: () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => EventDetails(event: widget.event),
          )),
          onLongPress: _profileBloc.currentUser.user.username == widget.event.host.user.username
              ? _showDeletionMenu
              : () => {},
          onTapDown: _getTapPosition,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8.0))
            ),
            child: ListTile(
                leading: ClipRRect(
                  borderRadius: BorderRadius.circular(200),
                  child: CachedNetworkImage(
                    imageUrl: widget.event.picture ?? '',
                    fit: BoxFit.cover,
                    width: 38,
                    height: 38,
                    placeholder: (context, url) => Icon(Icons.group, color: Theme.of(context).accentColor, size: 38,),
                    errorWidget: (context, url, error) => Icon(Icons.group, color: Theme.of(context).accentColor, size: 38),
                  ),
                ),
                title: Text(widget.event.name,
                    style: TextStyle(color: Theme.of(context).textTheme.bodyText1.color)),
                subtitle: Text(widget.event.host.user.username,
                    style: TextStyle(color: Theme.of(context).textTheme.bodyText1.color)),
                trailing: Icon(Icons.arrow_forward_ios,
                    color: Theme.of(context).textTheme.bodyText1.color)),
          ),
        ));
  }

  void _showDeletionMenu() {
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();

    showMenu(
        context: context,
        color: Theme.of(context).colorScheme.surface,
        items: <PopupMenuEntry>[
          CustomPopupMenuItem(
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
                showDialog(context: context, builder: (context) {
                  return ConfirmationDialog(
                    stringKey: "confirm_delete_event",
                    removedItem: widget.event.name,
                    callBackFunction: _deleteEvent,
                  );
                });
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(Lang.of(context).translate("delete_item"), style: TextStyle(
                      color: Theme.of(context).textTheme.bodyText1.color
                  )),
                  Container(
                    height: 20.0, width: 1.0,
                    color: Colors.grey[400].withOpacity(0.9),
                    margin: const EdgeInsets.only(left: 16.0, right: 16.0),
                  ),
                  Icon(Icons.delete, color: Theme.of(context).accentColor, size: 40.0)
                ],
              ),
            ),
          ),
        ],
        position: RelativeRect.fromRect(
            _tapPosition & Size(10, 10),
            Offset.zero & overlay.semanticBounds.size
        )
    );
  }

  void _getTapPosition(TapDownDetails details) {
    _tapPosition = details.globalPosition;
  }

  Future _deleteEvent() async {
    _eventBloc.removeEvent(widget.event.id);
  }
}
