import 'package:flutter/material.dart';
import 'package:splithere/localizations/app_localizations.dart';

class DialogSubmitButton extends StatelessWidget {

  final void Function() onPressed;

  DialogSubmitButton({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      elevation: 5.0,
      child: Text(
          Lang.of(context).translate('submit'),
          style: TextStyle(
              color: Theme.of(context).colorScheme.onPrimary,
              fontSize: 18.0,
              fontWeight: FontWeight.w500
          )),
      onPressed: onPressed
    );
  }
}
