import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/bill.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/screens/main/views/confirmation_dialog.dart';
import 'package:splithere/screens/main/widgets/receipt_detail.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/custom_popupmenu_item.dart';
import 'package:splithere/widgets/double_info_row.dart';
import 'package:splithere/widgets/single_info_row.dart';
import 'package:intl/intl.dart';

class ReceiptCard extends StatefulWidget {
  final Bill bill;
  final Event event;

  ReceiptCard({this.bill, this.event});

  @override
  _ReceiptCardState createState() => _ReceiptCardState();
}

class _ReceiptCardState extends State<ReceiptCard> {
  var _tapPosition;

  ProfileBloc _profileBloc;
  EventsBloc _eventsBloc;
  StreamSubscription _deleteReceiptSubscription;

  @override
  void initState() {
    super.initState();

    _profileBloc = AppModule.injector.getBloc();
    _eventsBloc = AppModule.injector.getBloc();

    _deleteReceiptSubscription = _profileBloc.deleteBillStream.listen((event) {
      showToast(context, Lang.of(context).translate('deleted'));
      _eventsBloc.getEventBills(widget.event.id);
      _eventsBloc.getEventLoans(widget.event.id);
      _eventsBloc.getEventDebts(widget.event.id);
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _deleteReceiptSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: GestureDetector(
        onTap: () =>
            Navigator.push(context, TransparentRoute(builder: (context) => ReceiptDetail(widget.bill))),
        onLongPress:
        (_profileBloc.currentUser.user.username == widget.event.host.user.username ||
            _profileBloc.currentUser.user.username == widget.bill.payer.user.username)
            ? _showDeletionMenu
            : () => {},
        onTapDown: _getTapPosition,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
          padding: EdgeInsets.only(top: 12.0, bottom: 12.0, left: 12.0),
          child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 100,
                      maxHeight: 100,
                    ),
                    child: Hero(
                        tag: widget.bill.id,
                        child: Container(
                          width: 75,
                          child: CachedNetworkImage(
                            imageUrl: widget.bill.photo ?? '',
                            width: 75,
                            fit: BoxFit.fitWidth,
                            placeholder: (context, url) => Center(
                              child: CircularProgressIndicator(),
                            ),
                            errorWidget: (context, url, error) => Icon(Icons.description, color: Theme.of(context).colorScheme.onPrimary, size: 40,),
                          ),
                        )
                    )
                ),
                Container(
                  height: 100.0, width: 1.0,
                  color: Colors.grey[400].withOpacity(0.9),
                  margin: const EdgeInsets.only(left: 12.0),
                ),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      DoubleRow(
                        label1: Lang.of(context).translate("total_amount"), text1: widget.bill.expense.toStringAsFixed(2),
                        label2: Lang.of(context).translate("payer"), text2: widget.bill.payer.user.username,
                        labelColor: Theme.of(context).accentColor,
                        textColor: Theme.of(context).textTheme.bodyText1.color, widthFactor: 0.92,
                        fontSize: 18.0, crossAxisAlignment: CrossAxisAlignment.start,
                        lowerFont: 2.0,
                      ),
                      FractionallySizedBox(
                        widthFactor: 0.92,
                        child: Divider(height: 12.0, thickness: 1.0,
                            color: Colors.grey[400].withOpacity(0.9)),
                      ),
                      SingleRow(
                        label: Lang.of(context).translate("added"),
                        text: widget.bill?.date != null ? DateFormat.yMd().add_jm().format(widget.bill.date) : '',
                        fontSize: 17.0, lowerFont: 2.0, widthFactor: 0.92,
                        labelColor: Theme.of(context).accentColor,
                        textColor: Theme.of(context).textTheme.bodyText1.color,
                        crossAxisAlignment: CrossAxisAlignment.start,
                      ),
                    ],
                  ),
                )
              ]
          ),
        ),
      ),
    );
  }

  void _showDeletionMenu() {
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();

    showMenu(
      context: context,
      color: Theme.of(context).colorScheme.surface,
      items: <PopupMenuEntry>[
        CustomPopupMenuItem(
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
              showDialog(context: context, builder: (context) {
                return ConfirmationDialog(
                  stringKey: "confirm_delete_bill",
                  removedItem: "${Lang.of(context).translate("receipt_with")}: ${widget.bill.expense}",
                  callBackFunction: _deleteReceipt,
                );
              });
            },
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(Lang.of(context).translate("delete_item"), style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText1.color
                )),
                Container(
                  height: 20.0, width: 1.0,
                  color: Colors.grey[400].withOpacity(0.9),
                  margin: const EdgeInsets.only(left: 16.0, right: 16.0),
                ),
                Icon(Icons.delete, color: Theme.of(context).accentColor, size: 40.0)
              ],
            ),
          ),
        ),
      ],
      position: RelativeRect.fromRect(
        _tapPosition & Size(10, 10),
        Offset.zero & overlay.semanticBounds.size
      )
    );
  }

  void _getTapPosition(TapDownDetails details) {
    _tapPosition = details.globalPosition;
  }

  Future _deleteReceipt() async {
    if (_profileBloc.currentUser.user.username == widget.event.host.user.username ||
        _profileBloc.currentUser.user.username == widget.bill.payer.user.username) {
      _profileBloc.deleteBill(widget.bill);
    }
  }
}

class TransparentRoute extends PageRoute<void> {
  TransparentRoute({
    @required this.builder,
    RouteSettings settings,
  })  : assert(builder != null),
        super(settings: settings, fullscreenDialog: false);

  final WidgetBuilder builder;

  @override
  bool get opaque => false;

  @override
  Color get barrierColor => null;

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  Duration get transitionDuration => Duration(milliseconds: 350);

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    final result = builder(context);
    return FadeTransition(
      opacity: Tween<double>(begin: 0, end: 1).animate(animation),
      child: Semantics(
        scopesRoute: true,
        explicitChildNodes: true,
        child: result,
      ),
    );
  }
}