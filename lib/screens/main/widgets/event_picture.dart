import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:splithere/styles/consts.dart';

class EventPicture extends StatelessWidget {

  final String imageUrl;
  final String eventName;

  EventPicture({this.imageUrl, this.eventName});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        CachedNetworkImage(
          imageUrl: imageUrl,
          placeholder: (context, url) => CircleAvatar(
            backgroundColor: Theme.of(context).primaryColorDark,
            radius: Consts.eventLogoRadius,
            child: CircularProgressIndicator(),
          ),
          imageBuilder: (context, image) => CircleAvatar(
            backgroundImage: image,
            radius: Consts.eventLogoRadius,
          ),
          errorWidget: (context, url, error) => CircleAvatar(
            backgroundColor: Theme.of(context).accentColor,
            radius: Consts.eventLogoRadius,
            child: Icon(Icons.image, size: 40, color: Theme.of(context).colorScheme.onPrimary,),
          ),
        ),
        SizedBox(height: 8.0),
        Text(eventName, style: TextStyle(
            fontWeight: FontWeight.w300,
            color: Theme.of(context).colorScheme.onPrimary,
            fontSize: 26.0
        )),
      ],
    );
  }
}
