import 'package:flutter/material.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/bill.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/screens/main/widgets/receipt_card.dart';

class BillsList extends StatefulWidget {
  final Event event;

  BillsList(this.event);

  @override
  State<StatefulWidget> createState() => BillsListState();
}

class BillsListState extends State<BillsList> {
  EventsBloc _eventsBloc;

  @override
  void initState() {
    super.initState();
    _eventsBloc = AppModule.injector.getBloc();
    _eventsBloc.getEventBills(widget.event.id);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Bill>>(
        stream: _eventsBloc.eventBillsStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return ReceiptCard(
                  bill: snapshot.data[index],
                  event: widget.event,
                );
              },
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text(Lang.of(context).translate('something_went_wrong')),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        }
    );
  }
}