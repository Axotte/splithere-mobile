import 'package:flutter/material.dart';

class DialogTextField extends StatelessWidget {

  final String label;
  final TextEditingController textController;
  final FocusNode focusNode;

  final String Function(String) validator;
  final void Function(String) onFieldSubmitted;

  DialogTextField({this.label, this.textController, this.focusNode,
    this.validator, this.onFieldSubmitted});

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 0.96,
      child: TextFormField(
        controller: textController,
        focusNode: focusNode,
        validator: validator,
        onFieldSubmitted: onFieldSubmitted,
        style: TextStyle(
          color: Theme.of(context).colorScheme.onPrimary,
          fontSize: 18.0,
        ),
        decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.only(bottom: 4.0),
            labelText: label,
            labelStyle: TextStyle(
                color: Theme.of(context).accentColor.withOpacity(0.8)),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color:
              Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color:
              Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color:
              Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
            )
        ),
      ),
    );
  }
}
