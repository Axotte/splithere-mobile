import 'package:flutter/material.dart';

class CustomFAB extends StatelessWidget {
  final void Function() onPressed;

  CustomFAB({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 16.0,
      right: 16.0,
      child: FloatingActionButton(
          shape: StadiumBorder(),
          elevation: 4,
          onPressed: onPressed,
          backgroundColor: Theme.of(context).accentColor,
          child: Icon( Icons.add, size: 26.0,
            color: Theme.of(context).textTheme.bodyText2.color, )),
    );
  }
}
