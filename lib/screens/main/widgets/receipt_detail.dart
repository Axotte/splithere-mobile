import 'package:cached_network_image/cached_network_image.dart';
import "package:flutter/material.dart";
import 'package:splithere/models/bill.dart';

class ReceiptDetail extends StatelessWidget {

  final Bill bill;

  ReceiptDetail(this.bill);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Center(
        child: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.88)
            ),
            padding: EdgeInsets.all(16),
            child: Hero(
              tag: bill.id,
              child: CachedNetworkImage(
                imageUrl: bill.photo ?? '',
                fit: BoxFit.fitWidth,
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Icon(Icons.description, color: Theme.of(context).colorScheme.onPrimary),
              ),
            ),
          ),
        ),
      )
    );
  }
}