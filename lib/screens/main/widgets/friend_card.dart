import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:splithere/blocs/friends_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/screens/main/views/confirmation_dialog.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/custom_popupmenu_item.dart';

class FriendListItem extends StatefulWidget {
  final Member member;
  FriendListItem(this.member);

  @override
  _FriendListItemState createState() => _FriendListItemState();
}

class _FriendListItemState extends State<FriendListItem> {
  var _tapPosition;

  FriendsBloc _friendsBloc;
  StreamSubscription _deleteFriendStream;


  @override
  void initState() {
    super.initState();

    _friendsBloc = AppModule.injector.getBloc();

    _deleteFriendStream = _friendsBloc.removeFriendStream.listen((event) {
      showToast(context, Lang.of(context).translate('deleted'));
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _deleteFriendStream.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: GestureDetector(
        onLongPress: _showDeletionMenu,
        onTapDown: _getTapPosition,
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8.0))
            ),
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 4.0),
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(200),
                child: CachedNetworkImage(
                  imageUrl: widget.member.avatar ?? '',
                  fit: BoxFit.cover,
                  width: 48,
                  height: 48,
                  placeholder: (context, url) => Icon(Icons.person, color: Theme.of(context).accentColor, size: 48),
                  errorWidget: (context, url, error) => Icon(Icons.person, color: Theme.of(context).accentColor, size: 48),
                ),
              ),
              title: Text(widget.member.user.username,
                  style: TextStyle(color: Theme.of(context).textTheme.bodyText1.color, fontSize: 22.0)),
            )),
      ),
    );
  }

  void _showDeletionMenu() {
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();

    showMenu(
        context: context,
        color: Theme.of(context).colorScheme.surface,
        items: <PopupMenuEntry>[
          CustomPopupMenuItem(
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
                showDialog(context: context, builder: (context) {
                  return ConfirmationDialog(
                    stringKey: "confirm_delete_friend",
                    removedItem: widget.member.user.username,
                    callBackFunction: _deleteFriend,
                  );
                });
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(Lang.of(context).translate("delete_item"), style: TextStyle(
                      color: Theme.of(context).textTheme.bodyText1.color
                  )),
                  Container(
                    height: 20.0, width: 1.0,
                    color: Colors.grey[400].withOpacity(0.9),
                    margin: const EdgeInsets.only(left: 16.0, right: 16.0),
                  ),
                  Icon(Icons.delete, color: Theme.of(context).accentColor, size: 40.0)
                ],
              ),
            ),
          ),
        ],
        position: RelativeRect.fromRect(
            _tapPosition & Size(10, 10),
            Offset.zero & overlay.semanticBounds.size
        )
    );
  }

  void _getTapPosition(TapDownDetails details) {
    _tapPosition = details.globalPosition;
  }

  Future _deleteFriend() async {
    _friendsBloc.removeFriend(widget.member.user);
  }
}
