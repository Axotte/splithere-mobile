import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/screens/main/views/confirmation_dialog.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/custom_popupmenu_item.dart';

class MemberCard extends StatefulWidget {
  final Member member;
  final Event event;
  MemberCard({this.member, this.event});

  @override
  _MemberCardState createState() => _MemberCardState();
}

class _MemberCardState extends State<MemberCard> {
  var _tapPosition;

  EventsBloc _eventsBloc;
  ProfileBloc _profileBloc;
  StreamSubscription _removeMemberSubscription;

  @override
  void initState() {
    super.initState();

    _eventsBloc = AppModule.injector.getBloc();
    _profileBloc = AppModule.injector.getBloc();

    _removeMemberSubscription = _eventsBloc.editEventStream.listen((event) {
      showToast(context, Lang.of(context).translate('deleted'));
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _removeMemberSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: GestureDetector(
        onLongPress:
        _profileBloc.currentUser.user.username == widget.event.host.user.username
            ? _showDeletionMenu
            : () => {},
        onTapDown: _getTapPosition,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8.0))
          ),
          padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
          child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: widget.member.avatar ?? '',
                  placeholder: (context, url) => CircleAvatar(
                    backgroundColor: Theme.of(context).primaryColorDark,
                    radius: 24,
                    child: CircularProgressIndicator(),
                  ),
                  imageBuilder: (context, image) => CircleAvatar(
                    backgroundImage: image,
                    radius: 24,
                  ),
                  errorWidget: (context, url, error) => CircleAvatar(
                      radius: 24,
                      backgroundColor: Theme.of(context).accentColor,
                      child: Icon(Icons.person, color: Theme.of(context).colorScheme.onPrimary)
                  ),
                ),
                Container(
                  height: 34.0, width: 1.6,
                  color: Colors.grey[400].withOpacity(0.9),
                  margin: const EdgeInsets.only(left: 12.0, right: 12.0),
                ),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(right: 16.0),
                    child: Text(widget.member.user?.username ?? '', style: TextStyle(
                        fontSize: 24.0, color: Theme.of(context).textTheme.bodyText1.color,
                        fontWeight: FontWeight.w400
                    )),
                  ),
                )
              ]
          ),
        ),
      ),
    );
  }

  void _showDeletionMenu() {
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();

    showMenu(
        context: context,
        color: Theme.of(context).colorScheme.surface,
        items: <PopupMenuEntry>[
          CustomPopupMenuItem(
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
                showDialog(context: context, builder: (context) {
                  return ConfirmationDialog(
                    stringKey: "confirm_delete_member",
                    removedItem: widget.member.user.username,
                    callBackFunction: _deleteMember,
                  );
                });
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(Lang.of(context).translate("delete_item"), style: TextStyle(
                      color: Theme.of(context).textTheme.bodyText1.color
                  )),
                  Container(
                    height: 20.0, width: 1.0,
                    color: Colors.grey[400].withOpacity(0.9),
                    margin: const EdgeInsets.only(left: 16.0, right: 16.0),
                  ),
                  Icon(Icons.delete, color: Theme.of(context).accentColor, size: 40.0)
                ],
              ),
            ),
          ),
        ],
        position: RelativeRect.fromRect(
            _tapPosition & Size(10, 10),
            Offset.zero & overlay.semanticBounds.size
        )
    );
  }

  void _getTapPosition(TapDownDetails details) {
    _tapPosition = details.globalPosition;
  }

  Future _deleteMember() async {
    if (_profileBloc.currentUser.user.username == widget.event.host.user.username ||
        _profileBloc.currentUser.user.username == widget.member.user.username) return;

    final eventToUpdate = Event(
        members: widget.event.members.map((e) => e.toAdd()).toList()
          ..removeWhere((element) => element.user.username == widget.member.user.username),
        id: widget.event.id
    );
    _eventsBloc.editEvent(eventToUpdate);
  }
}