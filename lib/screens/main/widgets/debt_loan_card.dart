import 'package:flutter/material.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/debt.dart';
import 'package:splithere/widgets/double_info_row.dart';
import 'package:splithere/widgets/single_info_row.dart';

class DebtLoanCard extends StatelessWidget {
  final Debt item;
  final String stringKey;

  DebtLoanCard({this.item, this.stringKey});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () => {},
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
          padding: EdgeInsets.only(top: 12.0, bottom: 12.0, left: 12.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: Column(
                  children: <Widget>[
                    DoubleRow(
                      label1: Lang.of(context).translate("lender"), text1: item.creditor.user.username,
                      label2: Lang.of(context).translate("debtor"), text2: item.debtor.user.username,
                      labelColor: Theme.of(context).accentColor,
                      textColor: Theme.of(context).textTheme.bodyText1.color, widthFactor: 0.96,
                      fontSize: 18.0, crossAxisAlignment: CrossAxisAlignment.start,
                      lowerFont: 2.0,
                    ),
                    FractionallySizedBox(
                      widthFactor: 0.96,
                      child: Divider(height: 12.0, thickness: 1.0,
                          color: Colors.grey[400].withOpacity(0.9)),
                    ),
                    SingleRow(
                      label: Lang.of(context).translate(stringKey),
                      text: item.debt.toStringAsFixed(2),
                      fontSize: 17.0, lowerFont: 2.0, widthFactor: 0.96,
                      labelColor: Theme.of(context).accentColor,
                      textColor: Theme.of(context).textTheme.bodyText1.color,
                      crossAxisAlignment: CrossAxisAlignment.start,
                    ),
                  ],
                ),
              )
            ]
          ),
        ),
      ),
    );
  }
}