import 'package:flutter/material.dart';
import 'package:splithere/styles/consts.dart';

class DialogLogo extends StatelessWidget {

  final IconData icon;

  DialogLogo({this.icon});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: Consts.dialogPadding,
      right: Consts.dialogPadding,
      child: CircleAvatar(
        backgroundColor: Theme.of(context).accentColor,
        radius: Consts.dialogLogoRadius,
        child: Icon(icon, size: Consts.dialogLogoRadius * 1.3,
            color: Theme.of(context).primaryColorDark),
      ),
    );
  }
}
