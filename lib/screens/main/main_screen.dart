import 'package:flutter/material.dart';
import 'package:splithere/screens/main/main_content.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Listener(
          onPointerDown: (_) => FocusScope.of(context).requestFocus(FocusNode()),
          child: MainContent()
      ),
    );
  }
}