import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/blocs/friends_bloc.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/datasources/remote/client/api_client.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/screens/main/views/add_event_dialog.dart';
import 'package:splithere/screens/main/views/add_friend_dialog.dart';
import 'package:splithere/screens/main/widgets/event_card.dart';
import 'package:splithere/screens/main/widgets/custom_fab.dart';
import 'package:splithere/screens/main/widgets/friend_card.dart';
import 'package:splithere/screens/main/views/user_edit_dialog.dart';
import 'package:splithere/styles/consts.dart';
import 'package:splithere/styles/dialog_consts.dart';
import 'package:websafe_svg/websafe_svg.dart';
import 'widgets/event_card.dart';

class MainContent extends StatefulWidget {
  @override
  _MainContentState createState() => _MainContentState();
}

class _MainContentState extends State<MainContent> with SingleTickerProviderStateMixin {
  ProfileBloc _profileBloc;
  FriendsBloc _friendsBloc;
  EventsBloc _eventsBloc;
  StreamSubscription _logoutSubscription;

  TabController _tabBarController;
  int _currentIndex = 0;

  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _friendsBloc = AppModule.injector.getBloc();
    _profileBloc = AppModule.injector.getBloc();
    _eventsBloc = AppModule.injector.getBloc();

    _friendsBloc.updateFriends();
    _eventsBloc.updateEvents();
    _logoutSubscription = _profileBloc.currentUserStream.listen((event) {
      if (event == null) {
        Navigator.of(context).pushReplacementNamed('/login');
      }
    });
    _scrollController = ScrollController();
    _tabBarController = TabController(initialIndex: _currentIndex, length: 2, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _logoutSubscription.cancel();
    _scrollController.dispose();
    _tabBarController.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
        ),
        child: SafeArea(
          child: Stack(
              children: <Widget>[
                Positioned(
                    top: MediaQuery.of(context).size.height * 0.02,
                    left: 160, right: 0,
                    child: Image.asset(
                      'assets/images/logo_symbol.png',
                      color: Theme.of(context).primaryColorLight,
                    )
                ),
                Column(
                  children: <Widget>[
                    _navigateLogOut(),
                    Expanded(
                      flex: 0,
                      child: _userInformation(),
                    ),
                    Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            Expanded(
                              flex: 0,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(12.0), topRight: Radius.circular(12.0)
                                  ),
                                  color: Theme.of(context).colorScheme.surface,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                      spreadRadius: 3,
                                      blurRadius: 9,
                                      offset: Offset(0, 5), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: TabBar(
                                  labelColor: Theme.of(context).textTheme.bodyText1.color,
                                  indicatorColor: Theme.of(context).accentColor,
                                  unselectedLabelColor: Theme.of(context).colorScheme.onSurface,
                                  controller: _tabBarController,
                                  tabs: [
                                    Tab(child: Text(Lang.of(context).translate('events'))),
                                    Tab(child: Text(Lang.of(context).translate('friends'))),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                color: Theme.of(context).colorScheme.error,
                                child: TabBarView(
                                  controller: _tabBarController,
                                  physics: NeverScrollableScrollPhysics(),
                                  children: <Widget>[
                                    _eventsList(),
                                    _friendsList()
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                    )
                  ],
                ),
                CustomFAB(
                  onPressed: () {
                    switch(_tabBarController.index){
                      case 0:
                        showDialog(context: context, builder: (context){return AddEventDialog();});
                        break;
                      case 1:
                        showDialog(context: context, builder: (context){return AddFriendDialog();});
                        break;
                    }
                  },
                ),
                Positioned(
                  child: InkWell(
                    onTap: showQRCode,
                    child: Container(
                      width: 64,
                      height: 64,
                      padding: const EdgeInsets.all(16.0),
                      child: WebsafeSvg.asset(
                        'assets/images/qr-code.svg',
                        color: Theme
                            .of(context)
                            .colorScheme
                            .onPrimary,
                      ),
                    ),
                  ),
                ),
              ]
          ),
        ),
      ),
    );
  }

  Widget _navigateLogOut() => Material(
    color: Colors.transparent,
    child: Padding(
      padding: EdgeInsets.only(top: 8.0, right: 12.0),
      child: Align(
        alignment: Alignment.centerRight,
        child: InkWell(
          onTap: logoutCurrentUser,
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Text(Lang.of(context).translate('logout'), style: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Theme.of(context).colorScheme.onPrimary,
                    fontSize: 21.0
                )),
              ),
              Icon(Icons.exit_to_app, size: 24.0, color: Theme.of(context).colorScheme.onPrimary),
            ],
          ),
        ),
      ),
    ),
  );

  Widget _userInformation() => StreamBuilder<Member>(
    stream: _profileBloc.currentUserStream,
    builder: (context, snapshot) {
      if (!snapshot.hasData) return Container();
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: snapshot.data?.avatar ?? '',
            placeholder: (context, url) => Padding(
              padding: EdgeInsets.symmetric(horizontal: Consts.avatarPadding, vertical: Consts.avatarPadding - 8),
              child: CircleAvatar(
                backgroundColor: Theme.of(context).primaryColorDark,
                radius: Consts.avatarRadius,
                child: CircularProgressIndicator(),
              ),
            ),
            imageBuilder: (context, image) => Stack(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: Consts.avatarPadding, vertical: Consts.avatarPadding - 8),
                  child: CircleAvatar(
                    backgroundImage: image,
                    radius: Consts.avatarRadius,
                  ),
                ),
                Positioned(
                  bottom: Consts.avatarPadding - 8,
                  right: Consts.avatarPadding,
                  child: InkWell(
                    onTap: () => showDialog(context: context, builder: (context){return UserEditDialog();}),
                    child: CircleAvatar(
                      radius: Consts.avatarEditRadius,
                      backgroundColor: Theme.of(context).accentColor,
                      child: Icon(Icons.edit, color: Theme.of(context).colorScheme.onPrimary)
                    ),
                  ),
                )
              ],
            ),
            errorWidget: (context, url, error) => Stack(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: Consts.avatarPadding, vertical: Consts.avatarPadding - 8),
                  child: Icon(Icons.account_circle, size: Consts.avatarRadius * 2,),
                ),
                Positioned(
                  bottom: Consts.avatarPadding - 8,
                  right: Consts.avatarPadding,
                  child: InkWell(
                    onTap: () => showDialog(context: context, builder: (context){return UserEditDialog();}),
                    child: CircleAvatar(
                        radius: Consts.avatarEditRadius,
                        backgroundColor: Theme.of(context).accentColor,
                        child: Icon(Icons.edit, color: Theme.of(context).colorScheme.onPrimary)
                    ),
                  ),
                )
              ],
            ),
          ),
          Text("${Lang.of(context).translate('welcome')}, ${snapshot.data?.user?.username ?? ''}!", style: TextStyle(
              fontWeight: FontWeight.w300,
              color: Theme.of(context).colorScheme.onPrimary,
              fontSize: 22.0
          )),
          SizedBox(height: Consts.avatarPadding - 4,)
        ],
      );
    }
  );

  Widget _eventsList() => StreamBuilder<List<Event>>(
    stream: _eventsBloc.eventsObservable,
    builder: (context, snapshot) {
      if(snapshot.hasData)
        {
          return ListView.builder(
            controller: _scrollController,
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return EventListItem(snapshot.data[index]);
            },
          );
        }else if(snapshot.hasError){
          return Center(
            child: Text(Lang.of(context).translate('something_went_wrong')),
          );
        }else{
          return Center(
            child:CircularProgressIndicator(),
          );
      }
    }
  );

  Widget _friendsList() => StreamBuilder<List<Member>>(
    stream: _friendsBloc.friendsStream,
    builder: (context, snapshot) {
      if(snapshot.hasData){
        return ListView.builder(
          controller: _scrollController,
          itemCount: snapshot.data.length,
          itemBuilder: (BuildContext context, int index) {
            return FriendListItem(snapshot.data[index]);
          },
        );
      }else if(snapshot.hasError){
        return Center(
          child: Text(Lang.of(context).translate('something_went_wrong'))
        );
      }else{
        return Center(
            child: CircularProgressIndicator(),
        );
      }
    }
  );

  void showQRCode() {
    showDialog(
      context: context,
      builder: (context) {
        return Center(
          child: AspectRatio(
            aspectRatio: 1,
            child: GestureDetector(
              onTap: Navigator.of(context).pop,
              child: Container(
                margin: const EdgeInsets.all(32),
                padding: const EdgeInsets.all(32),
                decoration: new BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(Consts.dialogPadding),
                  boxShadow: DialogConsts.DIALOG_SHADOW,
                ),
                child: QrImage(
                  data: json.encode(_profileBloc.currentUser.user.toJson()),
                  version: QrVersions.auto,
                ),
              ),
            ),
          ),
        );
      }
    );
  }
}

class RemoveScrollGlow extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}