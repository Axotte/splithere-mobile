import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/debt.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/screens/main/views/add_eventmember_dialog.dart';
import 'package:splithere/screens/main/views/add_eventreceipt_dialog.dart';
import 'package:splithere/screens/main/views/settings_event_dialog.dart';
import 'package:splithere/screens/main/widgets/bills_list.dart';
import 'package:splithere/screens/main/widgets/custom_fab.dart';
import 'package:splithere/screens/main/widgets/event_picture.dart';
import 'package:splithere/screens/main/widgets/member_card.dart';
import 'package:splithere/widgets/double_info_row.dart';
import 'package:splithere/widgets/expandable_list_view.dart';

class EventDetails extends StatefulWidget {
  final Event event;

  EventDetails({this.event});

  @override
  _EventDetailsState createState() => _EventDetailsState();
}

class _EventDetailsState extends State<EventDetails>
    with SingleTickerProviderStateMixin {
  EventsBloc _eventsBloc;
  ProfileBloc _profileBloc;

  TabController _tabBarController;
  int _currentIndex = 0;

  ScrollController _scrollController;
  bool shouldVisible = true;
  bool _isHost = false;
  // debts/loans tab
  // 0 - none, 1 - 1st list, 2 - 2nd list
  int _whichExpanded;

  static const Map<int, String> intsToStatuses = {
    0: "status_notstarted",
    1: "status_ongoing",
    2: "status_finished"
  };

  @override
  void initState() {
    super.initState();
    _profileBloc = AppModule.injector.getBloc();
    _eventsBloc = AppModule.injector.getBloc();
    refresh();
    _whichExpanded = 0;

    _scrollController = ScrollController();
    _tabBarController = TabController(initialIndex: _currentIndex, length: 3, vsync: this);
    _tabBarController.addListener(_tabControllerListener);
  }

  @override
  void dispose() {
    super.dispose();
    _eventsBloc.clearSubjects();
    _scrollController.dispose();
    _tabBarController.dispose();
  }

  void _tabControllerListener() {
    _tabBarController.index == 2 ? setState(() { shouldVisible = false; })
        : setState(() { shouldVisible = true; });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Event>(
      stream: _eventsBloc.getEventObservable(widget.event.id),
      initialData: widget.event,
      builder: (context, snapshot) {
        return Scaffold(
          body: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
            child: SafeArea(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: MediaQuery.of(context).size.height * 0.02,
                    left: 160, right: 0,
                    child: Image.asset(
                      'assets/images/logo_symbol.png',
                       color: Theme.of(context).primaryColorLight,
                    )
                  ),
                  Column(
                    children: <Widget>[
                      _navigateBack(),
                      Expanded(
                        flex: 0,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 32.0),
                          child: Column(
                              children: <Widget>[
                                EventPicture(
                                  imageUrl: snapshot.data?.picture ?? '',
                                  eventName: snapshot.data?.name ?? '',
                                ),
                                SizedBox(height: 12.0),
                                StreamBuilder<double>(
                                  stream: _eventsBloc.totalCostsStream,
                                  builder: (context, totalCostsSnapshot) {
                                    return StreamBuilder(
                                      stream: _profileBloc.currentUserStream,
                                      builder: (context, isHostSnapshot) {
                                        return DoubleRow(label1: "Status",
                                          text1: Lang.of(context).translate(intsToStatuses[snapshot.data.status]),
                                          label2: Lang.of(context).translate("total_amount"),
                                          text2: totalCostsSnapshot.data?.toStringAsFixed(2) ?? '---', fontSize: 17.0,
                                          labelColor: Theme.of(context).accentColor,
                                          textColor: Theme.of(context).colorScheme.onPrimary, widthFactor: 0.84,
                                          hasOptions: isHostSnapshot.data?.user?.username == widget.event.host.user.username,
                                          onPressed: () => showDialog(context: context, builder: (context) { return SettingsEventDialog(snapshot.data); }),
                                        );
                                      }
                                    );
                                  }
                                ),
                              ]
                          ),
                        ),
                      ),
                      Expanded(
                          flex: 1,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                flex: 0,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(12.0), topRight: Radius.circular(12.0)
                                    ),
                                    color: Theme.of(context).colorScheme.surface,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                        spreadRadius: 3,
                                        blurRadius: 9,
                                        offset: Offset(0, 5), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  child: TabBar(
                                    labelColor: Theme.of(context).textTheme.bodyText1.color,
                                    indicatorColor: Theme.of(context).accentColor,
                                    unselectedLabelColor: Theme.of(context).colorScheme.onSurface,
                                    controller: _tabBarController,
                                    tabs: [
                                      Tab(child: Text(Lang.of(context).translate('members_list'))),
                                      Tab(child: Text(Lang.of(context).translate('receipts'))),
                                      Tab(child: Text(Lang.of(context).translate('debts_loans'))),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(
                                  color: Theme.of(context).colorScheme.error,
                                  child: TabBarView(
                                    controller: _tabBarController,
                                    physics: NeverScrollableScrollPhysics(),
                                    children: <Widget>[
                                      _membersList(snapshot.data?.members),
                                      BillsList(snapshot.data),
                                      _debtsLoansList()
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                      )
                    ],
                  ),
                  Visibility(
                    visible: shouldVisible,
                    child: CustomFAB(
                      onPressed: () {
                        switch(_tabBarController.index){
                          case 0:
                            showDialog(context: context, builder: (context){return AddEventMemberDialog(event: snapshot.data,);});
                            break;
                          case 1:
                            showDialog(context: context, builder: (context){return AddEventReceiptDialog(refresh, snapshot.data.id);});
                            break;
                        }
                      }
                    ),
                  )
                ]
              ),
            ),
          ),
        );
      }
    );
  }

  void refresh() {
    _eventsBloc.getEventBills(widget.event.id);
    _eventsBloc.getEventLoans(widget.event.id);
    _eventsBloc.getEventDebts(widget.event.id);
  }

  Widget _navigateBack() => Material(
    color: Colors.transparent,
    child: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(flex: 0,
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios, size: 22.0,
                    color: Theme.of(context).colorScheme.onPrimary),
                onPressed: () => Navigator.pop(context),
              ),
            ),
            Expanded(flex: 1,
              child: Align(
                child: Text(Lang.of(context).translate('event_info'), style: TextStyle(
                  fontWeight: FontWeight.w300, fontSize: 20.0,
                  color: Theme.of(context).colorScheme.onPrimary,
                )),
              ),
            ),
            Expanded(flex: 0, child: IconButton(
              icon: Icon(Icons.gps_fixed, size: 22.0,
                  color: Theme.of(context).colorScheme.onPrimary),
              onPressed: () => {},
            )),
          ],
        )
    ),
  );

  Widget _membersList(List<Member> members) => ListView.builder(
  controller: _scrollController,
    itemCount: members?.length ?? 0,
    itemBuilder: (BuildContext context, int index) {
      return MemberCard(member: members[index], event: widget.event);
    },
  );

  Widget _debtsLoansList() => LayoutBuilder(
    builder: (context, constraints) {
      return Container(
        width: MediaQuery.of(context).size.width,
        height: constraints.maxHeight,
        child: StreamBuilder<List<Debt>>(
          stream: _eventsBloc.eventDebtsStream,
          builder: (context, debtsSnapshot) {
            return StreamBuilder<List<Debt>>(
              stream: _eventsBloc.eventLoansStream,
              builder: (context, loansSnapshot) {
                if (debtsSnapshot.hasData || loansSnapshot.hasData) {
                  final List<Widget> items = [];
                  if (debtsSnapshot.hasData && debtsSnapshot.data.isNotEmpty) {
                    items.add(ExpandableListView(
                      titleKey: "debts", cardStringKey: "debt",
                      expandedHeight: constraints.maxHeight - 100,
                      items: debtsSnapshot.data,
                      expanded: _whichExpanded == 1 ,
                      onTap: () {
                        setState(() {
                          _whichExpanded != 1 ? _whichExpanded = 1 :
                          _whichExpanded = 0;
                        });
                      },
                    ));
                  }
                  if (loansSnapshot.hasData && loansSnapshot.data.isNotEmpty) {
                    items.add(ExpandableListView(
                      titleKey: "loans", cardStringKey: "loan",
                      expandedHeight: constraints.maxHeight - 100,
                      items: loansSnapshot.data,
                      expanded: _whichExpanded == 2,
                      onTap: () {
                        setState(() {
                          _whichExpanded != 2 ? _whichExpanded = 2 : _whichExpanded = 0;
                        });
                      },
                    ));
                  }
                  return Column(
                    mainAxisSize: MainAxisSize.max,
                    children: items,
                  );
                } else if (loansSnapshot.hasError || debtsSnapshot.hasError) {
                  return Center(
                    child: Text(Lang.of(context).translate('something_went_wrong')),
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              }
            );
          }
        )
      );
    }
  );
}
