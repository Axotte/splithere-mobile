import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/bill.dart';
import 'package:splithere/screens/main/widgets/dialog_logo.dart';
import 'package:splithere/screens/main/widgets/dialog_submit_button.dart';
import 'package:splithere/styles/consts.dart';
import 'package:splithere/styles/dialog_consts.dart';
import 'package:splithere/styles/splithere_icons.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/dialog_popup.dart';

class AddEventReceiptDialog extends StatefulWidget {
  final void Function() refreshBills;
  final int eventId;

  AddEventReceiptDialog(this.refreshBills, this.eventId);

  @override
  _AddEventReceiptDialogState createState() => _AddEventReceiptDialogState();
}

class _AddEventReceiptDialogState extends State<AddEventReceiptDialog> {

  final _formKey = GlobalKey<FormState>();

  final TextEditingController _receiptAmountController = TextEditingController(text: "");
  EventsBloc _eventsBloc;
  ProfileBloc _profileBloc;
  StreamSubscription _addBillSubscription;

  ImagePicker picker = ImagePicker();
  File _image;

  void initState() {
    super.initState();
    _eventsBloc = AppModule.injector.getBloc();
    _profileBloc = AppModule.injector.getBloc();
    _addBillSubscription = _eventsBloc.addBillStream.listen((event) {
      widget.refreshBills();
      showToast(context, Lang.of(context).translate('added'));
      Navigator.of(context).pop();
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _addBillSubscription.cancel();
    _receiptAmountController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DialogPopup(child: dialogContent,);
  }

  Widget get dialogContent => Stack(
    children: <Widget>[
      Container(
        padding: DialogConsts.DIALOG_PADDING_ELSE,
        margin: EdgeInsets.only(top: Consts.dialogLogoRadius),
        decoration: new BoxDecoration(
            color: Theme.of(context).primaryColor,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(Consts.dialogPadding),
            boxShadow: DialogConsts.DIALOG_SHADOW
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min, // To make the card compact
          children: <Widget>[
            Text(Lang.of(context).translate('event_add_receipt'),
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 8.0),
            _image == null ? Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                InkWell(
                  onTap: _selectImage,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.image, size: 64.0,
                        color: Theme.of(context).colorScheme.onPrimary.withOpacity(0.3)),
                      SizedBox(height: 2.0),
                      Text(Lang.of(context).translate("select_image"), style: TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 16.0
                      )),
                    ],
                  ),
                ),
                Text(Lang.of(context).translate("or"), style: TextStyle(
                  fontSize: 22.0, color: Theme.of(context).accentColor.withOpacity(0.9)
                )),
                InkWell(
                  onTap: _takePhoto,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.camera, size: 64.0,
                          color: Theme.of(context).colorScheme.onPrimary.withOpacity(0.3)),
                      SizedBox(height: 2.0),
                      Text(Lang.of(context).translate("take_photo"), style: TextStyle(
                          fontWeight: FontWeight.w500, fontSize: 16.0
                      )),
                    ],
                  ),
                )
              ],
            ) : Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
              ),
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.24,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(16.0),
                    image: DecorationImage(
                      image: FileImage(_image),
                      fit: BoxFit.scaleDown
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 16.0),
            FractionallySizedBox(
              widthFactor: 0.96,
              child: Form(
                key: _formKey,
                child: TextFormField(
                  controller: _receiptAmountController,
                  validator: (String value) {
                    if (value.isEmpty) return Lang.of(context).translate('receipt_amount_empty');
                    if (double.tryParse(value) == null) return Lang.of(context).translate('not_a_number');
                    return null;
                  },
                  onFieldSubmitted: (_) {
                    onSubmit();
                  },
                  keyboardType: TextInputType.number,
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.onPrimary,
                    fontSize: 18.0,
                  ),
                  decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.only(bottom: 4.0),
                      labelText: Lang.of(context).translate("receipt_amount"),
                      labelStyle: TextStyle(
                          color: Theme.of(context).accentColor.withOpacity(0.8)),
                      border: UnderlineInputBorder(
                        borderSide: BorderSide(color:
                        Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color:
                        Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color:
                        Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
                      )
                  ),
                ),
              ),
            ),
            SizedBox(height: 12.0),
            StreamBuilder<bool>(
              stream: _eventsBloc.loadingStream,
              initialData: false,
              builder: (context, snapshot) {
                if (snapshot.data) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Align(
                    alignment: Alignment.bottomRight,
                    child: DialogSubmitButton(
                      onPressed: onSubmit,
                    ),
                  );
                }
              }
            ),
          ],
        ),
      ),
      DialogLogo(icon: SplithereIcons.logo),
    ],
  );

  onSubmit() async {
    if (_formKey.currentState.validate()) {
      _eventsBloc.addBill(widget.eventId, Bill(
        payer: _profileBloc.currentUser,
        date: DateTime.now().toUtc(),
        expense: double.parse(_receiptAmountController.text),
        photo: base64.encode(await _image.readAsBytes())
      ));
    }
  }

  Future _selectImage() async {
    await picker.getImage(source: ImageSource.gallery).then((image) {
      setState(() { if (image != null) _image = File(image.path); });
    });
  }

  Future _takePhoto() async {
    await picker.getImage(source: ImageSource.camera).then((image) {
      setState(() { if (image != null) _image = File(image.path); });
    }).catchError((e) {
      print(e);
    });
  }
}