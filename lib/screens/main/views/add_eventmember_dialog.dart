import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/blocs/friends_bloc.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/screens/main/widgets/dialog_logo.dart';
import 'package:splithere/screens/main/widgets/dialog_submit_button.dart';
import 'package:splithere/styles/consts.dart';
import 'package:splithere/styles/dialog_consts.dart';
import 'package:splithere/styles/splithere_icons.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/dialog_popup.dart';
import 'package:splithere/widgets/searchable_dropdown.dart';

class AddEventMemberDialog extends StatefulWidget {
  final Event event;

  AddEventMemberDialog({this.event});

  @override
  _AddEventMemberDialogState createState() => _AddEventMemberDialogState();
}

class _AddEventMemberDialogState extends State<AddEventMemberDialog> {
  final TextEditingController _eventNameController = TextEditingController(text: "");
  final FocusNode _eventNameFocusNode = FocusNode();
  ProfileBloc _profileBloc;
  EventsBloc _eventsBloc;
  FriendsBloc _friendsBloc;
  StreamSubscription _friendsSubscription;
  StreamSubscription _editEventSubscription;

  List<int> selectedItems = [];
  final List<DropdownMenuItem> items = [];

  void initState() {
    super.initState();
    _profileBloc = AppModule.injector.getBloc();
    _eventsBloc = AppModule.injector.getBloc();
    _friendsBloc = AppModule.injector.getBloc();
    _friendsSubscription = _friendsBloc.friendsStream.listen((friends) {
      var membersNames = widget.event.members.map((e) => e.user.username).toList();
      for (var i = 0; i < friends.length; i++) {
        if (membersNames.contains(friends[i].user.username)) {
          selectedItems.add(i);
        }
      }
    });
    _editEventSubscription = _eventsBloc.editEventStream.listen((event) {
      showToast(context, Lang.of(context).translate('added'));
      Navigator.of(context).pop();
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _editEventSubscription.cancel();
    _friendsSubscription.cancel();
    _eventNameController.dispose();
    _eventNameFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DialogPopup(child: dialogContent,);
  }

  Widget get dialogContent => StreamBuilder<List<Member>>(
    stream: _friendsBloc.friendsStream,
    builder: (context, snapshot) {
      return Stack(
        children: <Widget>[
          Container(
            padding: DialogConsts.DIALOG_PADDING_ELSE,
            margin: EdgeInsets.only(top: Consts.dialogLogoRadius),
            decoration: new BoxDecoration(
                color: Theme.of(context).primaryColor,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(Consts.dialogPadding),
                boxShadow: DialogConsts.DIALOG_SHADOW
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min, // To make the card compact
              children: <Widget>[
                Text(Lang.of(context).translate('event_add_members'),
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 12.0),
                SearchableDropdown.multiple(
                  items: snapshot.data?.map((e) => DropdownMenuItem(
                    child: Text(e.user.username),
                    value: e.user.username,
                  ))?.toList() ?? [],
                  selectedItems: selectedItems,
                  hint: Text(Lang.of(context).translate("select_members"),
                    style: TextStyle(color: Theme.of(context).colorScheme.onPrimary, fontSize: 18.0)
                  ),
                  searchHint: Text(Lang.of(context).translate("select_members"),
                    style: TextStyle(color: Theme.of(context).accentColor, fontSize: 18.0)
                  ),
                  displayClearIcon: true,
                  onChanged: (value) {
                    setState(() { selectedItems = value; });
                  },
                  isExpanded: true,
                ),
                SizedBox(height: 12.0),
                StreamBuilder<bool>(
                  stream: _eventsBloc.loadingStream,
                  initialData: false,
                  builder: (context, isLoadingSnapshot) {
                    if (isLoadingSnapshot.data) {
                      return Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      return Align(
                        alignment: Alignment.bottomRight,
                        child: DialogSubmitButton(
                          onPressed: () => onSubmit(snapshot.data),
                        ),
                      );
                    }
                  }
                ),
              ],
            ),
          ),
          DialogLogo(icon: SplithereIcons.logo),
        ],
      );
    }
  );

  onSubmit(List<Member> friends) {
    final selectedFriends = selectedItems.map((e) => friends[e].toAdd()).toList();
    final eventToUpdate = Event(
      members: selectedFriends..add(_profileBloc.currentUser.toAdd()),
      id: widget.event.id
    );
    _eventsBloc.editEvent(eventToUpdate);
  }
}