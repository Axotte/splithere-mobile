import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/screens/main/widgets/dialog_submit_button.dart';
import 'package:splithere/styles/consts.dart';
import 'package:splithere/styles/dialog_consts.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/dialog_popup.dart';
import 'package:websafe_svg/websafe_svg.dart';

class UserEditDialog extends StatefulWidget {
  @override
  _UserEditDialogState createState() => _UserEditDialogState();
}

class _UserEditDialogState extends State<UserEditDialog> {

  final _formKey = GlobalKey<FormState>();
  ProfileBloc _profileBloc;
  ImagePicker _picker = ImagePicker();
  final phoneNumberController = TextEditingController();
  final accountNumberController = TextEditingController();
  final phoneNumberFocusNode = FocusNode();
  StreamSubscription _currentUserDataSubscription;
  StreamSubscription _updateUserSubscription;

  File _selectedImage;

  Pattern patternPhoneNumber = r'\d{9}';
  Pattern patternBankAccountNumber = r'\d{26}';

  RegExp regexPhoneNumber;
  RegExp regexBankAccountNumber;

  void initState() {
    super.initState();
    _profileBloc = AppModule.injector.getBloc();
    _currentUserDataSubscription = _profileBloc.currentUserStream.listen((event) {
      if ((event.phoneNumber ?? '').isNotEmpty) {
        phoneNumberController.value = TextEditingValue(text: event.phoneNumber);
      }
      if ((event.accountNumber ?? '').isNotEmpty) {
        accountNumberController.value = TextEditingValue(text: event.accountNumber);
      }
    });
    _updateUserSubscription = _profileBloc.updateUserStream.listen((event) {
      showToast(context, Lang.of(context).translate('edited'));
      Navigator.of(context).pop();
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });

    regexPhoneNumber = new RegExp(patternPhoneNumber);
    regexBankAccountNumber = new RegExp(patternBankAccountNumber);
  }


  @override
  Widget build(BuildContext context) {
    return DialogPopup(child: dialogContent,);
  }

  @override
  void dispose() {
    super.dispose();
    _currentUserDataSubscription.cancel();
    _updateUserSubscription.cancel();
  }

  Widget get dialogContent => StreamBuilder<Member>(
    stream: _profileBloc.currentUserStream,
    builder: (context, snapshot) {
      return Stack(
        children: <Widget>[
          Container(
            padding: DialogConsts.DIALOG_PADDING_AVATAR,
            margin: EdgeInsets.only(top: Consts.avatarRadius),
            decoration: new BoxDecoration(
              color: Theme.of(context).primaryColor,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(Consts.dialogPadding),
              boxShadow: DialogConsts.DIALOG_SHADOW,
            ),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min, // To make the card compact
                children: <Widget>[
                  Text(Lang.of(context).translate('user_edit'),
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(height: 12.0),
                  TextFormField(
                    controller: accountNumberController,
                    validator: (String value) {
                      if ((regexBankAccountNumber.hasMatch(value)&&regexBankAccountNumber.stringMatch(value)==value)||value.isEmpty) return null;
                      return Lang.of(context).translate('invalid_bank_account_number');
                    },
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).requestFocus(phoneNumberFocusNode);
                    },
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.onPrimary,
                      fontSize: 18.0,
                    ),
                    decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(bottom: 4.0),
                        labelText: Lang.of(context).translate('account_number'),
                        labelStyle: TextStyle(
                            color: Theme.of(context).accentColor.withOpacity(0.8)),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color:
                          Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color:
                          Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color:
                          Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
                        )
                    ),
                  ),
                  SizedBox(height: 12.0),
                  TextFormField(
                    controller: phoneNumberController,
                    validator: (String value) {
                      if ((regexPhoneNumber.hasMatch(value)&&regexPhoneNumber.stringMatch(value)==value)||value.isEmpty) return null;
                      return Lang.of(context).translate('invalid_phone_number');
                    },
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).unfocus();
                      onSubmit(snapshot.data);
                    },
                    focusNode: phoneNumberFocusNode,
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.onPrimary,
                      fontSize: 18.0,
                    ),
                    decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(bottom: 4.0),
                        labelText: Lang.of(context).translate('phone_number'),
                        labelStyle: TextStyle(
                            color: Theme.of(context).accentColor.withOpacity(0.8)),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color:
                          Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color:
                          Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color:
                          Theme.of(context).colorScheme.onPrimary.withOpacity(0.8)),
                        )
                    ),
                  ),
                  SizedBox(height: 12.0),
                  StreamBuilder<bool>(
                    stream: _profileBloc.loadingStream,
                    initialData: false,
                    builder: (context, isLoading) {
                      if (isLoading.data) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 18),
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        return Align(
                          alignment: Alignment.bottomRight,
                          child: DialogSubmitButton(
                            onPressed: () => onSubmit(snapshot.data),
                          ),
                        );
                      }
                    }
                  ),
                ],
              ),
            ),
          ),
          _avatarLogo(snapshot.data),
        ],
      );
    }
  );

  Widget _avatarLogo(Member currentUser) => Positioned(
    left: Consts.avatarPadding,
    right: Consts.avatarPadding,
    child: CircleAvatar(
      backgroundColor: Theme.of(context).accentColor,
      radius: Consts.avatarRadius,
      child: InkWell(
        onTap: _selectImage,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        child: _selectedImage != null ? _fileImage() : CachedNetworkImage(
          imageUrl: currentUser?.avatar ?? '',
          placeholder: (context, url) => CircleAvatar(
            backgroundColor: Theme.of(context).primaryColorDark,
            radius: Consts.avatarRadius,
            child: CircularProgressIndicator(),
          ),
          imageBuilder: (context, image) => Stack(
              children: <Widget> [
                CircleAvatar(
                  backgroundImage: image,
                  radius: Consts.avatarRadius,
                ),
                Positioned(
                  top: Consts.avatarPadding + 8.0,
                  left: Consts.avatarPadding + 8.0,
                  child: WebsafeSvg.asset(
                    'assets/images/focus.svg',
                    color: Theme.of(context).primaryColorLight.withOpacity(0.6),
                    width: 64.0,
                  ),
                )
              ]
          ),
          errorWidget: (context, url, error) => CircleAvatar(
              backgroundColor: Theme.of(context).primaryColorDark,
              radius: Consts.avatarRadius,
              child: WebsafeSvg.asset(
                'assets/images/focus.svg',
                color: Theme.of(context).colorScheme.onPrimary.withOpacity(0.3),
                width: 54.0,
              )
          ),
        ),
      ),
    ),
  );

  Widget _fileImage() {
    return Stack(
        children: <Widget> [
          CircleAvatar(
            backgroundImage: FileImage(_selectedImage),
            radius: Consts.avatarRadius,
          ),
          Positioned(
            top: Consts.avatarPadding + 8.0,
            left: Consts.avatarPadding + 8.0,
            child: WebsafeSvg.asset(
              'assets/images/focus.svg',
              color: Theme.of(context).primaryColorLight.withOpacity(0.6),
              width: 64.0,
            ),
          )
        ]
    );
  }

  _selectImage() {
    _picker.getImage(source: ImageSource.gallery).then((image) {
      setState(() { if (image != null) _selectedImage = File(image.path); });
    });
  }

  onSubmit(Member member) async {
    if (_formKey.currentState.validate()) {
      final newPhoneNumber = phoneNumberController.text;
      final newAccountNumber = accountNumberController.text;

      final userToUpdate = Member();

      if (newAccountNumber != null) {
        userToUpdate.accountNumber = newAccountNumber;
      }
      if (newPhoneNumber != null) {
        userToUpdate.phoneNumber = newPhoneNumber;
      }
      if (_selectedImage != null) {
        userToUpdate.avatar = base64.encode(await _selectedImage.readAsBytes());
      }
      _profileBloc.updateUser(userToUpdate);
      FocusScope.of(context).unfocus();
    }
  }
}