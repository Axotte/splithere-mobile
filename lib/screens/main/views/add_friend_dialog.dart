import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:splithere/blocs/friends_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/user/user.dart';
import 'package:splithere/screens/main/widgets/dialog_submit_button.dart';
import 'package:splithere/screens/main/widgets/dialog_text_field.dart';
import 'package:splithere/styles/consts.dart';
import 'package:splithere/styles/dialog_consts.dart';
import 'package:splithere/styles/splithere_icons.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/dialog_popup.dart';
import 'package:websafe_svg/websafe_svg.dart';

import '../widgets/dialog_logo.dart';

class AddFriendDialog extends StatefulWidget {
  AddFriendDialog();

  @override
  _AddFriendDialogState createState() => _AddFriendDialogState();
}

class _AddFriendDialogState extends State<AddFriendDialog> {
  final _formKey = GlobalKey<FormState>();
  FriendsBloc _friendBloc;

  StreamSubscription _addFriendStream;
  final usernameController = TextEditingController();

  void initState() {
    super.initState();
    _friendBloc = AppModule.injector.getBloc();

    _addFriendStream = _friendBloc.addFriendStream.listen((event) {
      showToast(context, Lang.of(context).translate('added'));
      Navigator.of(context).pop();
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _addFriendStream.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return DialogPopup(child: dialogContent);
  }

  Widget get dialogContent => Stack(
    children: <Widget>[
      Container(
        padding: DialogConsts.DIALOG_PADDING_ELSE,
        margin: EdgeInsets.only(top: Consts.dialogLogoRadius),
        decoration: new BoxDecoration(
          color: Theme
              .of(context)
              .primaryColor,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(Consts.dialogPadding),
          boxShadow: DialogConsts.DIALOG_SHADOW,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min, // To make the card compact
          children: <Widget>[
            Text(Lang.of(context).translate('add_friend'),
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 8.0),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  DialogTextField(
                    label: Lang.of(context).translate(
                        'friend_insert'),
                    textController: usernameController,
                    validator: (String username) {
                      if(username.isNotEmpty) return null;
                      return Lang.of(context).translate('event_name_empty');
                    },
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).unfocus();
                      onSubmit();
                    },
                  )
                ],
              ),
            ),
            SizedBox(height: 12.0),
            StreamBuilder<bool>(
              stream: _friendBloc.loadingStream,
              initialData: false,
              builder: (context, snapshot) {
                if (snapshot.data) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Column(
                    children: <Widget>[
                      Align(
                          alignment: Alignment.bottomRight,
                          child: DialogSubmitButton(
                              onPressed: () => onSubmit()
                          )
                      ),
                      SizedBox(height: 16.0),
                      InkWell(
                        onTap: onScan,
                        child: Container(
                          padding: EdgeInsets.only(right: 16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text("or use QR Code", style: TextStyle(
                                  fontSize: 18.0, color: Theme
                                  .of(context)
                                  .colorScheme
                                  .onPrimary)),
                              SizedBox(width: 12.0),
                              WebsafeSvg.asset(
                                'assets/images/qr-code.svg',
                                color: Theme
                                    .of(context)
                                    .colorScheme
                                    .onPrimary,
                                width: 32.0,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                }
              }
            )
          ],
        ),
      ),
      DialogLogo(icon: SplithereIcons.logo),
    ],
  );

  onSubmit() async {
    if (_formKey.currentState.validate()) {

      final newUserName = usernameController.text;

      final friendToAdd = User();

      if (newUserName != null) {
        friendToAdd.username = newUserName;
      }

      _friendBloc.addFriend(friendToAdd);
      FocusScope.of(context).unfocus();
    }
  }

  onScan() async {
    String scannedCode = await FlutterBarcodeScanner.scanBarcode("#ff6666", "Cancel", true, ScanMode.QR);
    try {
      final user = User.fromJson(json.decode(scannedCode));
      _friendBloc.addFriend(user);
    } catch (e) {
      showToast(context, Lang.of(context).translate('invalid_qr'));
    }
  }
}
