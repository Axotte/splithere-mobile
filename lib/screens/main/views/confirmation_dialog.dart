import 'dart:async';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:splithere/blocs/friends_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/screens/main/widgets/dialog_submit_button.dart';
import 'package:splithere/styles/consts.dart';
import 'package:splithere/styles/dialog_consts.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/dialog_popup.dart';

import '../widgets/dialog_logo.dart';

class ConfirmationDialog extends StatefulWidget {
  final String stringKey;
  final String removedItem;
  final void Function() callBackFunction;
  ConfirmationDialog({this.stringKey, this.removedItem, this.callBackFunction});

  @override
  _ConfirmationDialogState createState() => _ConfirmationDialogState();
}

class _ConfirmationDialogState extends State<ConfirmationDialog> {
  final _formKey = GlobalKey<FormState>();
  FriendsBloc _friendBloc;

  StreamSubscription _addFriendStream;
  final usernameController = TextEditingController();

  void initState() {
    super.initState();
    _friendBloc = AppModule.injector.getBloc();

    _addFriendStream = _friendBloc.addFriendStream.listen((event) {
      showToast(context, Lang.of(context).translate('added'));
      Navigator.of(context).pop();
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _addFriendStream.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return DialogPopup(child: dialogContent);
  }

  Widget get dialogContent => Stack(
    children: <Widget>[
      Container(
        padding: DialogConsts.DIALOG_PADDING_ELSE,
        margin: EdgeInsets.only(top: Consts.dialogLogoRadius),
        decoration: new BoxDecoration(
          color: Theme
              .of(context)
              .primaryColor,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(Consts.dialogPadding),
          boxShadow: DialogConsts.DIALOG_SHADOW,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min, // To make the card compact
          children: <Widget>[
            FractionallySizedBox(
              widthFactor: 0.94,
              child: Text(Lang.of(context).translate(widget.stringKey),
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(height: 16.0),
            FractionallySizedBox(
              widthFactor: 0.94,
              child: Text(widget.removedItem,
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            SizedBox(height: 16.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Align(
                  alignment: Alignment.bottomRight,
                    child: MaterialButton(
                      elevation: 5.0,
                      child: Text(
                        Lang.of(context).translate('cancel'),
                        style: TextStyle(
                            color: Theme.of(context).colorScheme.onPrimary,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500
                        )),
                      onPressed: () => Navigator.pop(context)
                    ),
                  ),
                ),
                Expanded(
                  child: Align(
                      alignment: Alignment.bottomRight,
                      child: DialogSubmitButton(
                          onPressed: () {
                            widget.callBackFunction();
                            Navigator.pop(context);
                          }
                      )
                  ),
                ),
              ],
            ),
            SizedBox(height: 16.0),
          ],
        ),
      ),
      DialogLogo(icon: Icons.error_outline),
    ],
  );
}
