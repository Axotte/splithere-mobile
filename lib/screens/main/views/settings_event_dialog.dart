import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/screens/main/widgets/dialog_submit_button.dart';
import 'package:splithere/screens/main/widgets/dialog_text_field.dart';
import 'package:splithere/styles/consts.dart';
import 'package:splithere/styles/dialog_consts.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/dialog_popup.dart';
import 'package:websafe_svg/websafe_svg.dart';


class SettingsEventDialog extends StatefulWidget {
  final Event event;

  SettingsEventDialog(this.event);

  @override
  _SettingsEventDialogState createState() => _SettingsEventDialogState();
}

class _SettingsEventDialogState extends State<SettingsEventDialog> {
  final _formKey = GlobalKey<FormState>();
  ImagePicker _picker = ImagePicker();
  File _selectedImage;
  EventsBloc _eventsBloc;
  StreamSubscription _editEventSubscription;

  final TextEditingController _eventNameController = TextEditingController(text: "");
  final FocusNode _eventNameFocusNode = FocusNode();

  String _dropDownValue;

  static const Map<String, int> statusesToInts = {
    "status_notstarted": 0,
    "status_ongoing": 1,
    "status_finished": 2
  };

  static const Map<int, String> intsToStatuses = {
    0: "status_notstarted",
    1: "status_ongoing",
    2: "status_finished"
  };

  void initState() {
    super.initState();
    _eventsBloc = AppModule.injector.getBloc();
    _editEventSubscription = _eventsBloc.editEventStream.listen((event) {
      showToast(context, Lang.of(context).translate('edited'));
      Navigator.of(context).pop();
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });
    _eventNameController.value = TextEditingValue(text: widget.event.name);
    // change to currentstatus
    _dropDownValue = intsToStatuses[widget.event.status];
  }

  @override
  void dispose() {
    super.dispose();
    _editEventSubscription.cancel();
    _eventNameController.dispose();
    _eventNameFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DialogPopup(child: dialogContent);
  }

  Widget get dialogContent => Stack(
    children: <Widget>[
      Container(
        padding: DialogConsts.DIALOG_PADDING_ELSE,
        margin: EdgeInsets.only(top: Consts.dialogLogoRadius),
        decoration: new BoxDecoration(
          color: Theme.of(context).primaryColor,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(Consts.dialogPadding),
          boxShadow: DialogConsts.DIALOG_SHADOW,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min, // To make the card compact
          children: <Widget>[
            SizedBox(height: 16.0),
            Column(
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: DialogTextField(
                    textController: _eventNameController,
                    focusNode: _eventNameFocusNode,
                    label: Lang.of(context).translate("event_name_edit"),
                    validator: (String eventName) {
                      if(eventName.isNotEmpty) return null;
                      return Lang.of(context).translate('event_empty');
                    },
                    onFieldSubmitted: (_) {},
                  ),
                ),
                SizedBox(height: 32.0),
                FractionallySizedBox(
                  widthFactor: 0.96,
                  child: Theme(
                    data: Theme.of(context).copyWith(
                      canvasColor: Theme.of(context).primaryColorLight,
                    ),
                    child: DropdownButtonFormField(
                      isExpanded: true,
                      value: _dropDownValue,
                      items: ["status_notstarted", "status_ongoing", "status_finished"]
                          .map<DropdownMenuItem<String>>((value) {
                        return DropdownMenuItem(
                          value: value,
                          child: Text(Lang.of(context).translate(value)),
                        );
                      }).toList(),
                      onChanged: (String newValue) {
                        setState(() { _dropDownValue = newValue; });
                      },
                      style: TextStyle(color: Theme.of(context).colorScheme.onPrimary, fontSize: 17.0),
                      iconEnabledColor: Theme.of(context).colorScheme.onPrimary.withOpacity(0.6),
                      iconDisabledColor: Theme.of(context).colorScheme.onPrimary.withOpacity(0.6),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Theme.of(context).accentColor),
                        icon: Icon(Icons.event_note, size: 24.0,
                          color: Theme.of(context).colorScheme.onPrimary
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color:
                          Theme.of(context).colorScheme.onPrimary.withOpacity(0.5)
                        )),
                        isDense: true,
                        contentPadding: EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 12.0),
                        fillColor: Theme.of(context).colorScheme.onPrimary,
                      ),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 12.0),
            StreamBuilder<bool>(
              stream: _eventsBloc.loadingStream,
              initialData: false,
              builder: (context, snapshot) {
                if (snapshot.data) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Align(
                      alignment: Alignment.bottomRight,
                      child: DialogSubmitButton(
                        onPressed: onConfirm,
                      )
                  );
                }
              }
            ),
          ],
        ),
      ),
      _eventLogo()
    ],
  );

  onConfirm() async {
    if (_formKey.currentState.validate()) {
      String newName = _eventNameController.text;
      Event event = Event();
      if ((newName ?? '').isNotEmpty) {
        event.name = newName;
      }
      if (_selectedImage != null) {
        event.picture = base64.encode(
            await _selectedImage.readAsBytes());
    }
    event.status = statusesToInts[_dropDownValue];
    event.id = widget.event.id;
    _eventsBloc.editEvent(event);
    }
  }

  Widget _eventLogo() => Positioned(
    left: Consts.eventPadding,
    right: Consts.eventPadding,
    child: CircleAvatar(
      backgroundColor: Theme.of(context).accentColor,
      radius: Consts.eventLogoRadius,
      child: InkWell(
        onTap: _selectImage,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        child: _selectedImage != null ? _fileImage() : CachedNetworkImage(
          imageUrl: widget.event?.picture ?? '',
          placeholder: (context, url) => CircleAvatar(
            backgroundColor: Theme.of(context).primaryColorDark,
            radius: Consts.eventLogoRadius,
            child: CircularProgressIndicator(),
          ),
          imageBuilder: (context, image) => Stack(
              children: <Widget> [
                CircleAvatar(
                  backgroundImage: image,
                  radius: Consts.eventLogoRadius,
                ),
                Positioned(
                  top: Consts.eventPadding - 2,
                  left: Consts.eventPadding- 2,
                  child: WebsafeSvg.asset(
                    'assets/images/focus.svg',
                    color: Theme.of(context).primaryColorLight.withOpacity(0.5),
                    width: 48.0,
                  ),
                )
              ]
          ),
          errorWidget: (context, url, error) => CircleAvatar(
              backgroundColor: Theme.of(context).primaryColorDark,
              radius: Consts.eventLogoRadius,
              child: WebsafeSvg.asset(
                'assets/images/focus.svg',
                color: Theme.of(context).colorScheme.onPrimary.withOpacity(0.3),
                width: 48.0,
              )
          ),
        ),
      ),
    ),
  );

  Widget _fileImage() {
    return Stack(
        children: <Widget> [
          CircleAvatar(
            backgroundImage: FileImage(_selectedImage),
            radius: Consts.eventLogoRadius,
          ),
          Positioned(
            top: Consts.eventPadding - 2,
            left: Consts.eventPadding - 2,
            child: WebsafeSvg.asset(
              'assets/images/focus.svg',
              color: Theme.of(context).primaryColorLight.withOpacity(0.5),
              width: 48.0,
            ),
          )
        ]
    );
  }

  _selectImage() {
    _picker.getImage(source: ImageSource.gallery).then((image) {
      setState(() { if (image != null) _selectedImage = File(image.path); });
    });
  }
}
