import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:splithere/blocs/events_bloc.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/screens/main/widgets/dialog_logo.dart';
import 'package:splithere/screens/main/widgets/dialog_submit_button.dart';
import 'package:splithere/screens/main/widgets/dialog_text_field.dart';
import 'package:splithere/styles/consts.dart';
import 'package:splithere/styles/dialog_consts.dart';
import 'package:splithere/styles/splithere_icons.dart';
import 'package:splithere/utils/toast_util.dart';
import 'package:splithere/widgets/dialog_popup.dart';

class AddEventDialog extends StatefulWidget {
  @override
  _AddEventDialogState createState() => _AddEventDialogState();
}

class _AddEventDialogState extends State<AddEventDialog> {

  final _formKey = GlobalKey<FormState>();
  EventsBloc _eventBloc;
  StreamSubscription _addEventStream;

  final TextEditingController _eventNameController = TextEditingController(text: "");
  final FocusNode _eventNameFocusNode = FocusNode();

  void initState() {
    super.initState();
    _eventBloc = AppModule.injector.getBloc();

    _addEventStream = _eventBloc.addEventStream.listen((event) {
      showToast(context, Lang.of(context).translate('added'));
      Navigator.of(context).pop();
    }, onError: (e) {
      showToast(context, Lang.of(context).translate('something_went_wrong'));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _addEventStream.cancel();
    _eventNameController.dispose();
    _eventNameFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DialogPopup(child: dialogContent,);
  }

  Widget get dialogContent => Stack(
    children: <Widget>[
      Container(
        padding: DialogConsts.DIALOG_PADDING_ELSE,
        margin: EdgeInsets.only(top: Consts.dialogLogoRadius),
        decoration: new BoxDecoration(
          color: Theme.of(context).primaryColor,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(Consts.dialogPadding),
          boxShadow: DialogConsts.DIALOG_SHADOW
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min, // To make the card compact
          children: <Widget>[
            Text(Lang.of(context).translate('add_event'),
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 8.0),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  DialogTextField(
                    label: Lang.of(context).translate('event_insert'),
                    textController: _eventNameController,
                    focusNode: _eventNameFocusNode,
                    validator: (String username) {
                      if(username.isNotEmpty) return null;
                      return Lang.of(context).translate('event_name_empty');
                    },
                    onFieldSubmitted: (_) {},
                  )
                ],
              ),
            ),
            SizedBox(height: 12.0),
            StreamBuilder<bool>(
              stream: _eventBloc.loadingStream,
              initialData: false,
              builder: (context, snapshot) {
                if (snapshot.data) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Align(
                    alignment: Alignment.bottomRight,
                    child: DialogSubmitButton(
                        onPressed: () => onSubmit()
                    ),
                  );
                }
              }
            ),
          ],
        ),
      ),
      DialogLogo(icon: SplithereIcons.logo),
    ],
  );

  onSubmit() async {
    if (_formKey.currentState.validate()) {

    final newEventName = _eventNameController.text;

    final eventToAdd = Event();

    if (newEventName != null) {
      eventToAdd.name = newEventName;
      eventToAdd.status = 0;
    }

    _eventBloc.addEvent(eventToAdd);
    FocusScope.of(context).unfocus();
    }
  }
}