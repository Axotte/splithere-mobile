import 'package:flutter/material.dart';
import 'package:splithere/screens/login/login_content.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Listener(
          onPointerDown: (_) => FocusScope.of(context).requestFocus(FocusNode()),
          child: LoginContent()
      ),
    );
  }
}