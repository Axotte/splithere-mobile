import 'dart:async';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/enums/auth_screen_state.dart';
import 'package:splithere/screens/login/widgets/login_widget.dart';
import 'package:splithere/screens/login/widgets/register_widget.dart';
import 'package:splithere/screens/main/main_screen.dart';
import 'package:splithere/utils/logger.dart';

import '../../blocs/events_bloc.dart';
import '../../injection/app_module.dart';

class LoginContent extends StatefulWidget {
  @override
  _LoginContentState createState() => _LoginContentState();
}

class _LoginContentState extends State<LoginContent> {
  ProfileBloc _profileBloc;
  StreamSubscription _currentProfileSubscription;
  AuthScreenState _screenState = AuthScreenState.LOGIN;

  @override
  void initState() {
    super.initState();
    _profileBloc = AppModule.injector.getBloc();
    _currentProfileSubscription = _profileBloc.currentUserStream.listen((event) {
      if (event != null) {
        Navigator.of(context).pushReplacementNamed('/main');
      }
    }, onError: (_) {});
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
      ),
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          LoginWidget(
            isVisible: _screenState == AuthScreenState.LOGIN,
            onActionButtonPressed: _changeScreenType,
          ),
          RegisterWidget(
            isVisible: _screenState == AuthScreenState.REGISTER,
            onActionButtonPressed: _changeScreenType,
          ),
        ]
      )
    );
  }

  @override
  void dispose() {
    super.dispose();
    _currentProfileSubscription.cancel();
  }

  void _changeScreenType(AuthScreenState state) {
    if (_screenState == state) return;

    setState(() { _screenState = state; });
  }
}