import 'package:flutter/material.dart';

class FormFieldLabel extends StatelessWidget {
  final String text;

  FormFieldLabel({this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 30.0, top: 4.0),
      child: Text(text, style: TextStyle(
          color: Colors.grey, fontSize: 16.0
        ),
      ),
    );
  }
}
