import 'package:flutter/material.dart';


typedef void OnClick();

class NoAccountButton extends StatelessWidget {
  final String text;
  final OnClick onPressed;

  NoAccountButton({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30.0,
      margin: const EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: FlatButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24.0)),
              highlightColor: Theme.of(context).primaryColor,
              color: Colors.transparent,
              child: Container(
                padding: const EdgeInsets.only(left: 20.0),
                alignment: Alignment.center,
                child: Text(
                  text.toUpperCase(),
                  style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
                ),
              ),
              onPressed: onPressed,
            ),
          ),
        ],
      ),
    );
  }
}
