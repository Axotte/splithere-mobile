import 'package:flutter/material.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/blocs/register_validation_bloc.dart';
import 'package:splithere/enums/auth_screen_state.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/injection/login_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/screens/login/widgets/no_account_button.dart';
import 'package:splithere/widgets/form_field_button.dart';
import 'package:splithere/widgets/input_form_field.dart';

class RegisterWidget extends StatefulWidget {
  final bool isVisible;
  final void Function(AuthScreenState state) onActionButtonPressed;

  RegisterWidget({this.isVisible, this.onActionButtonPressed});

  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {
  RegisterValidationBloc _registerValidationBloc;
  ProfileBloc _profileBloc;
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _repeatPasswordController = TextEditingController();
  final FocusNode _userNameFocusNode = FocusNode();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();
  final FocusNode _repeatPasswordFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _registerValidationBloc = LoginModule.injector.getBloc();
    _profileBloc = AppModule.injector.getBloc();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      left: widget.isVisible ? 0 : MediaQuery.of(context).size.width,
      duration: Duration(milliseconds: 300),
      curve: Curves.fastOutSlowIn,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.transparent,
          alignment: Alignment.center,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Spacer(),
              _logo(),
              Spacer(),
              InputTextField(
                controller: _userNameController,
                focusNode: _userNameFocusNode,
                onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(_passwordFocusNode),
                icon: Icons.person_outline,
                obscure: false,
                hint: Lang.of(context).translate('username_hint'),
                label: Lang.of(context).translate('username'),
                errorStream: _registerValidationBloc.loginStream,
                onChange: _registerValidationBloc.changeLogin,
              ),
              InputTextField(
                controller: _emailController,
                focusNode: _emailFocusNode,
                onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(_passwordFocusNode),
                icon: Icons.alternate_email,
                obscure: false,
                hint: Lang.of(context).translate('email_hint'),
                label: Lang.of(context).translate('email'),
                errorStream: _registerValidationBloc.emailStream,
                onChange: _registerValidationBloc.changeEmail,
              ),
              InputTextField(
                focusNode: _passwordFocusNode,
                controller: _passwordController,
                onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(_repeatPasswordFocusNode),
                icon: Icons.lock_outline,
                obscure: true,
                hint: Lang.of(context).translate('password_hint'),
                label: Lang.of(context).translate('password'),
                errorStream: _registerValidationBloc.passwordStream,
                onChange: _registerValidationBloc.changePassword,
              ),
              InputTextField(
                focusNode: _repeatPasswordFocusNode,
                controller: _repeatPasswordController,
                onFieldSubmitted: (_) => onSubmit(),
                icon: Icons.lock,
                obscure: true,
                hint: Lang.of(context).translate('password_repeat'),
                label: Lang.of(context).translate('password_repeat'),
                errorStream: _registerValidationBloc.repeatPasswordStream,
                onChange: _registerValidationBloc.changeRepeatPassword,
              ),
              StreamBuilder<bool>(
                stream: _profileBloc.loadingStream,
                initialData: false,
                builder: (context, snapshot) {
                  if (snapshot.data) {
                    return Padding(
                      padding: const EdgeInsets.all(16),
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return FormFieldButton(
                      icon: Icons.arrow_forward,
                      text: Lang.of(context).translate('register'),
                      mainColor: Theme.of(context).backgroundColor,
                      textColor: Theme.of(context).textTheme.bodyText2.color,
                      splashColor: Colors.blue[900],
                      onPressed: onSubmit,
                    );
                  }
                }
              ),
              NoAccountButton(
                text: Lang.of(context).translate('already_have_account'),
                onPressed: onViewChange,
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _logo() => Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Image.asset(
        'assets/images/logo_symbol.png',
        color: Theme.of(context).colorScheme.onPrimary,
        height: MediaQuery.of(context).size.longestSide * 0.19,
        width: MediaQuery.of(context).size.longestSide * 0.19,
      ),
    ],
  );

  void onSubmit() {
    FocusScope.of(context).unfocus();
    final tokenRequest = _registerValidationBloc.validateAndGetRequest();
    if (tokenRequest != null) {
      _profileBloc.register(tokenRequest);
    }
  }

  void onViewChange() {
    FocusScope.of(context).unfocus();
    widget.onActionButtonPressed(AuthScreenState.LOGIN);
  }
}
