import 'package:flutter/material.dart';
import 'package:splithere/blocs/login_validation_bloc.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/enums/auth_screen_state.dart';
import 'package:splithere/injection/app_module.dart';
import 'package:splithere/injection/login_module.dart';
import 'package:splithere/localizations/app_localizations.dart';
import 'package:splithere/screens/login/widgets/no_account_button.dart';
import 'package:splithere/widgets/form_field_button.dart';
import 'package:splithere/widgets/input_form_field.dart';

import '../../main/main_screen.dart';
import '../../main/views/event_details.dart';

class LoginWidget extends StatefulWidget {

  final bool isVisible;
  final void Function(AuthScreenState state) onActionButtonPressed;

  LoginWidget({this.isVisible, this.onActionButtonPressed});

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  LoginValidationBloc _loginValidationBloc;
  ProfileBloc _profileBloc;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _loginValidationBloc = LoginModule.injector.getBloc();
    _profileBloc = AppModule.injector.getBloc();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      left: widget.isVisible ? 0 : -MediaQuery.of(context).size.width,
      duration: Duration(milliseconds: 300),
      curve: Curves.fastOutSlowIn,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.transparent,
          alignment: Alignment.center,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _logo(),
              SizedBox(height: 8.0),
              InputTextField(
                controller: _emailController,
                focusNode: _emailFocusNode,
                onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(_passwordFocusNode),
                icon: Icons.person_outline,
                obscure: false,
                hint: Lang.of(context).translate('email_hint'),
                label: Lang.of(context).translate('email'),
                onChange: _loginValidationBloc.changeEmail,
                errorStream: _loginValidationBloc.emailStream,
              ),
              InputTextField(
                controller: _passwordController,
                focusNode: _passwordFocusNode,
                onFieldSubmitted: (_) => onSubmit(),
                icon: Icons.lock_open,
                obscure: true,
                hint: Lang.of(context).translate('password_hint'),
                label: Lang.of(context).translate('password'),
                onChange: _loginValidationBloc.changePassword,
                errorStream: _loginValidationBloc.passwordStream,
              ),
              StreamBuilder<bool>(
                  stream: _profileBloc.loadingStream,
                  initialData: false,
                  builder: (context, snapshot) {
                    if (snapshot.data) {
                      return Padding(
                        padding: const EdgeInsets.all(16),
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      return FormFieldButton(
                        icon: Icons.arrow_forward,
                        text: Lang.of(context).translate('log_in'),
                        mainColor: Theme.of(context).accentColor,
                        textColor: Theme.of(context).textTheme.bodyText2.color,
                        splashColor: Colors.red[900],
                        onPressed: onSubmit,
                      );
                    }
                  }
              ),
              NoAccountButton(
                text: Lang.of(context).translate('no_account'),
                onPressed: onViewChange,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _logo() => Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Image.asset(
        'assets/images/logo_symbol.png',
        color: Theme.of(context).colorScheme.onPrimary,
        height: MediaQuery.of(context).size.longestSide * 0.26,
        width: MediaQuery.of(context).size.longestSide * 0.26,
      ),
      Image.asset(
        'assets/images/splithere_text.png',
        color: Theme.of(context).accentColor,
        width: MediaQuery.of(context).size.longestSide * 0.36,
      ),
    ],
  );

  void onSubmit() {
    FocusScope.of(context).unfocus();

    final tokenRequest = _loginValidationBloc.validateAndGetRequest();
    if (tokenRequest != null) {
      _profileBloc.login(tokenRequest);
    }
  }

  void onViewChange() {
    FocusScope.of(context).unfocus();
    // goto
    widget.onActionButtonPressed(AuthScreenState.REGISTER);
  }
}
