import 'dart:async';

import 'package:flutter/material.dart';
import 'package:splithere/blocs/profile_bloc.dart';
import 'package:splithere/injection/app_module.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  ProfileBloc _profileBloc;
  StreamSubscription _currentUserSubscription;

  @override
  void initState() {
    super.initState();
    _profileBloc = AppModule.injector.getBloc();
    _profileBloc.refreshCurrentUser();
    _currentUserSubscription = _profileBloc.currentUserStream.listen((event) {
      if (event != null) {
        Navigator.of(context).pushReplacementNamed('/main');
      } else {
        Navigator.of(context).pushReplacementNamed('/login');
      }
    }, onError: (e) {
      Navigator.of(context).pushReplacementNamed('/login');
    });
  }

  @override
  Widget build(BuildContext context) {
   return Scaffold(
     backgroundColor: Theme.of(context).primaryColor,
     body: SizedBox.expand(
       child: Column(
         mainAxisSize: MainAxisSize.max,
         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
         children: <Widget>[
           Image.asset(
             'assets/images/logo_symbol.png',
             color: Theme.of(context).colorScheme.onPrimary,
             height: MediaQuery.of(context).size.longestSide * 0.26,
             width: MediaQuery.of(context).size.longestSide * 0.26,
           ),
           Image.asset(
             'assets/images/splithere_text.png',
             color: Theme.of(context).accentColor,
             width: MediaQuery.of(context).size.longestSide * 0.36,
           ),
           CircularProgressIndicator()
         ],
       ),
     ),
   );
  }

  @override
  void dispose() {
    super.dispose();
    _currentUserSubscription.cancel();
  }
}