import 'package:splithere/datasources/local/friends_dao.dart';
import 'package:splithere/datasources/remote/friends_api_provider.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/models/user/user.dart';

class FriendsRepository {
  FriendsApiProvider _friendsApiProvider;
  FriendsDao _friendsDao;

  FriendsRepository(this._friendsDao, this._friendsApiProvider);

  Future<List<Member>> getAllFriends() => _friendsApiProvider.getFriends().then((friends) {
    _friendsDao.replaceAll(friends);
    return friends;
  }).catchError((_) => _friendsDao.getAll());

  Future addFriend(User friend) => _friendsApiProvider.addFriend(friend);

  Future removeFriend(User friend) => _friendsApiProvider.removeFriend(friend);

  Future clearDatabase() => _friendsDao.removeAll();
}