import 'dart:io';

import 'package:splithere/datasources/local/auth_shared_preferences.dart';
import 'package:splithere/datasources/local/profile_dao.dart';
import 'package:splithere/datasources/remote/auth_api_provider.dart';
import 'package:splithere/datasources/remote/profile_api_provider.dart';
import 'package:splithere/models/bill.dart';
import 'package:splithere/models/debt.dart';
import 'package:splithere/models/token/token_request.dart';
import 'package:splithere/models/user/member.dart';
import 'package:dio/dio.dart';

class ProfileRepository {
  ProfileApiProvider _profileApiProvider;
  AuthApiProvider _authApiProvider = AuthApiProvider();
  ProfileDao _profileDao;

  ProfileRepository(this._profileApiProvider, this._profileDao);

  Future<Member> getCurrentUser() => _profileApiProvider.currentUser().then((profile) {
    _profileDao.setCurrentUser(profile);
    return profile;
  }).catchError((e) {
    if (e is DioError) {
      if (e.type == DioErrorType.CONNECT_TIMEOUT || e.type == DioErrorType.RECEIVE_TIMEOUT || e.type == DioErrorType.SEND_TIMEOUT) {
        return _profileDao.getCurrentUser();
      } else if (e.type == DioErrorType.DEFAULT && e is SocketException) {
        return _profileDao.getCurrentUser();
      }
    }
    throw e;
  });

  Future login(TokenRequest tokenRequest) async {
    final token = await _authApiProvider.getToken(tokenRequest);
    await AuthSharedPreferences.saveToken(token);
    return getCurrentUser();
  }

  Future register(TokenRequest tokenRequest) async {
    final token = await _authApiProvider.register(tokenRequest);
    await AuthSharedPreferences.saveToken(token);
    return getCurrentUser();
  }

  Future updateUser(Member member) => _profileApiProvider.updateUser(member);

  Future<List<Bill>> getAllMyBills() => _profileApiProvider.getAllMyBills();

  Future deleteBill(int billId) => _profileApiProvider.deleteBill(billId);

  Future<List<Debt>> getAllMyDebts() => _profileApiProvider.getAllMyDebts();

  Future<List<Debt>> getAllMyLoans() => _profileApiProvider.getAllMyLoans();

  Future updateDebtStatus(int debtId, Debt debt) => _profileApiProvider.updateDebtStatus(debtId, debt);

  Future updateLoanStatus(int loanId, Debt loan) => _profileApiProvider.updateLoanStatus(loanId, loan);

  clearDatabase() {
    AuthSharedPreferences.clearToken();
    _profileDao.removeCurrentUser();
  }

}