import 'package:splithere/datasources/local/events_dao.dart';
import 'package:splithere/datasources/remote/events_api_provider.dart';
import 'package:splithere/models/bill.dart';
import 'package:splithere/models/debt.dart';
import 'package:splithere/models/event.dart';

class EventsRepository {
  EventsDao _eventsDao;
  EventsApiProvider _eventsApiProvider;

  EventsRepository(this._eventsApiProvider, this._eventsDao);

  Future<List<Event>> getAllEvents() => _eventsApiProvider.getAllEvents().then((events) {
    _eventsDao.replaceAll(events);
    return events;
  }).catchError((_) => _eventsDao.getAll());

  Future createEvent(Event event) => _eventsApiProvider.createEvent(event);

  Future updateEvent(Event event) => _eventsApiProvider.updateEvent(event, event.id);

  Future deleteEvent(int eventId) => _eventsApiProvider.deleteEvent(eventId);

  Future<List<Bill>> getEventBills(eventId) => _eventsApiProvider.getEventBills(eventId);

  Future addBill(eventId, Bill bill) => _eventsApiProvider.addBill(eventId, bill);

  Future<List<Debt>> getEventDebts(eventId) => _eventsApiProvider.getEventDebts(eventId);

  Future<List<Debt>> getEventLoans(eventId) => _eventsApiProvider.getEventLoans(eventId);

  Future clearDatabase() => _eventsDao.removeAll();
}