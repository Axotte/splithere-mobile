import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:splithere/datasources/local/auth_shared_preferences.dart';
import 'package:splithere/datasources/remote/auth_api_provider.dart';
import 'package:splithere/models/token/token.dart';
import 'package:splithere/models/token/token_request.dart';
import 'package:splithere/utils/logger.dart';


Dio _apiClient;

PublishSubject _logoutSubject = PublishSubject();
Stream get logoutStream => _logoutSubject.stream;

logoutCurrentUser() => _logoutSubject.add(null);

Dio getApiClient() {
  if (_apiClient == null) {
    _apiClient = Dio();
    _apiClient.interceptors.add(_interceptor);
  }
  return _apiClient;
}

InterceptorsWrapper get _interceptor => InterceptorsWrapper(
  onRequest: _onRequest,
  onResponse: _onResponse,
  onError: _onError
);

Future<RequestOptions> _onRequest(RequestOptions options) async {
  _apiClient.lock();
  logger.i('Request: ${options.path}');
  Token token = await AuthSharedPreferences.getToken();
  if (token == null) {
    _apiClient.clear();
    _apiClient.unlock();
    return options;
  }
  if (token.expireDateInMillis - DateTime.now().millisecondsSinceEpoch < 0) {
    try {
      Token refreshedToken = await AuthApiProvider().getToken(TokenRequest.fromToken(token));
      await AuthSharedPreferences.saveToken(refreshedToken);
      token = refreshedToken;
    } catch (e) {
      DioError error = e;
      logger.w('Failed to refresh token', e);
      if (error.response.statusCode == 400) {
        logger.wtf('Logout');
        _logoutSubject.add(null);
        _apiClient.clear();
        _apiClient.unlock();
        return options;
      }
    }
  }
  _apiClient.unlock();
  return options..headers['Authorization'] = '${token.tokenType} ${token.accessToken}';
}

Future<Response> _onResponse(Response response) async {
  logger.i('Response: (${response.statusCode}) - ${response.request.path}');
  return response;
}

Future<DioError> _onError(DioError error) async {
  logger.w('Error: (${error.request.path})', error.error);
  return error;
}



