import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:splithere/config/app_config.dart';
import 'package:splithere/models/event.dart';
import 'package:splithere/models/bill.dart';
import 'package:splithere/models/debt.dart';

part 'events_api_provider.g.dart';

@RestApi(baseUrl: AppConfig.baseUrl)
abstract class EventsApiProvider {
  factory EventsApiProvider(Dio dio, {String baseUrl}) = _EventsApiProvider;

  @GET('/api/events/')
  Future<List<Event>> getAllEvents();

  @POST('/api/events/')
  Future<void> createEvent(@Body() Event event);

  @PATCH('/api/events/{eventId}')
  Future<void> updateEvent(@Body() Event event, @Path() eventId);

  @DELETE('/api/events/{eventId}')
  Future<void> deleteEvent(@Path() eventId);

  @GET('/api/events/{eventId}/bills')
  Future<List<Bill>> getEventBills(@Path() eventId);

  @POST('/api/events/{eventId}/bills')
  Future<void> addBill(@Path() eventId, @Body() Bill bill);

  @GET('/api/events/{eventId}/debts')
  Future<List<Debt>> getEventDebts(@Path() eventId);

  @GET('/api/events/{eventId}/loans')
  Future<List<Debt>> getEventLoans(@Path() eventId);
}