import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:splithere/config/app_config.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/models/bill.dart';
import 'package:splithere/models/debt.dart';
import 'package:splithere/models/user/user.dart';

part 'profile_api_provider.g.dart';

@RestApi(baseUrl: AppConfig.baseUrl)
abstract class ProfileApiProvider {
  factory ProfileApiProvider(Dio dio, {String baseUrl}) = _ProfileApiProvider;

  @GET('/api/me/')
  Future<Member> currentUser();

  @PATCH('/api/me/')
  Future<void> updateUser(@Body() Member user);

  @GET('/api/me/bills')
  Future<List<Bill>> getAllMyBills();

  @DELETE('/api/me/bills/{billId}')
  Future<void> deleteBill(@Path() int billId);

  @GET('/api/me/debts')
  Future<List<Debt>> getAllMyDebts();

  @GET('/api/me/loans')
  Future<List<Debt>> getAllMyLoans();

  @PATCH('/api/me/debts/{debtId}')
  Future<void> updateDebtStatus(@Path() int debtId, @Body() Debt debt);

  @PATCH('/api/me/debts/{loanId}')
  Future<void> updateLoanStatus(@Path() int loanId, @Body() Debt loan);

}