import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:splithere/config/app_config.dart';
import 'package:splithere/models/user/member.dart';
import 'package:splithere/models/user/user.dart';

part 'friends_api_provider.g.dart';

@RestApi(baseUrl: AppConfig.baseUrl)
abstract class FriendsApiProvider {
  factory FriendsApiProvider(Dio dio, {String baseUrl}) = _FriendsApiProvider;

  @GET('/api/friends')
  Future<List<Member>> getFriends();

  @POST('/api/friends')
  Future<void> addFriend(@Body() User friend);

  @DELETE('/api/friends')
  Future<void> removeFriend(@Body() User friend);
}