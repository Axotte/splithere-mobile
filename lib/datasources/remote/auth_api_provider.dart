import 'package:retrofit/http.dart';
import 'package:dio/dio.dart';
import 'package:splithere/config/app_config.dart';
import 'package:splithere/models/token/token.dart';
import 'package:splithere/models/token/token_request.dart';

part 'auth_api_provider.g.dart';

@RestApi(baseUrl: AppConfig.baseUrl)
abstract class AuthApiProvider {
  static final AuthApiProvider _instance = AuthApiProvider._internal(Dio());
  factory AuthApiProvider._internal(Dio dio, {String baseUrl}) = _AuthApiProvider;
  factory AuthApiProvider() => _instance;

  @POST('/o/token/')
  Future<Token> getToken(@Body() TokenRequest tokenRequest);

  @POST('/user/register/')
  Future<Token> register(@Body() TokenRequest tokenRequest);
}