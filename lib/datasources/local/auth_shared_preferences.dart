import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:splithere/models/token/token.dart';

class AuthSharedPreferences {
  static const String TOKEN_KEY = '_TOKEN_KEY';

  static Future<bool> saveToken(Token token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(TOKEN_KEY, jsonEncode(token.toJson()));
  }

  static Future<Token> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String tokenString = prefs.getString(TOKEN_KEY);
    if (tokenString == null) return null;
    Map<String, dynamic> token = jsonDecode(tokenString);
    return Token.fromJson(token);
  }

  static Future<bool> clearToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(TOKEN_KEY, null);
  }
}
