import 'dart:async';

import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:path/path.dart';
import 'package:sembast/sembast_io.dart';

class AppDatabase {
  AppDatabase._();
  static AppDatabase instance = AppDatabase._();

  Completer<Database> _db;

  Future<Database> get db async {
    if (_db == null) {
      _db = Completer();
      _openDatabase();
    }
    return _db.future;
  }

  Future _openDatabase() async {
    final appDocumentDir = await getApplicationDocumentsDirectory();
    final dbPath = join(appDocumentDir.path, 'StudentsDB.db');
    final database = await databaseFactoryIo.openDatabase(dbPath);
    _db.complete(database);
  }
}