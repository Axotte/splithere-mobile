import 'package:sembast/sembast.dart';
import 'package:splithere/datasources/local/base_dao.dart';
import 'package:splithere/models/user/member.dart';

class FriendsDao extends BaseDao<Member> {
  @override
  StoreRef store = intMapStoreFactory.store('Friends');

  @override
  Member fromJson(Map<String, dynamic> json) {
    return Member.fromJson(json);
  }
}