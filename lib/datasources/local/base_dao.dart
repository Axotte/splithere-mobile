import 'package:sembast/sembast.dart';
import 'package:splithere/datasources/local/database.dart';

abstract class BaseDao<T> {
  Future<Database> get db async => await AppDatabase.instance.db;

  StoreRef store;

  T fromJson(Map<String, dynamic> json);

  Future replaceAll(List<dynamic> items) async {
    (await db).transaction((database) async {
      await store.delete(database);
      await store.addAll(database, items.map((item) => item.toJson()).toList());
    });
  }

  Future removeAll() async {
    await store.delete(await db);
  }

  Future<List<T>> getAll() async {
    final recordSnapshot = await store.find(await db);
    return recordSnapshot.map((snapshot) {
      return fromJson(snapshot.value);
    }).toList();
  }
}