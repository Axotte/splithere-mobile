import 'package:sembast/sembast.dart';
import 'package:splithere/datasources/local/base_dao.dart';
import 'package:splithere/models/event.dart';

class EventsDao extends BaseDao<Event> {
  @override
  StoreRef store = intMapStoreFactory.store('Events');

  @override
  Event fromJson(Map<String, dynamic> json) {
    return Event.fromJson(json);
  }
}