import 'package:sembast/sembast.dart';
import 'package:splithere/datasources/local/database.dart';
import 'package:splithere/models/user/member.dart';

class ProfileDao {
  Future<Database> get db async => await AppDatabase.instance.db;
  var store = StoreRef.main();

  Future<Member> getCurrentUser() async {
    return store.record('currentUser').get(await db).then((user) {
      if (user == null) return null;
      else return Member.fromJson(user);
    }).catchError((_) => null);
  }

  Future setCurrentUser(Member user) async {
    return store.record('currentUser').put(await db, user.toJson());
  }

  Future removeCurrentUser() async {
    return store.record('currentUser').delete(await db);
  }
}