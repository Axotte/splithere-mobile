class AppAnimationInfo {
  static const ACTION_BUTTON_DELAY = const Duration(milliseconds: 750);
  static const TWEEN_ANIMATION_DURATION = const Duration(milliseconds: 500);
}