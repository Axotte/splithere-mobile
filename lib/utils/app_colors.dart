import 'package:flutter/material.dart';

Map applicationColors = {
  'backgroundColor': Color(0xFF1C2433),
  'accentColor': Color(0xFFDA3A47),
  'accentColor2': Color(0xFF2E86AB),
  'textDark': Color(0xFF1A1A1A),
  'highlightColor': Color(0xFFF6F4F3),
  'detailsColor': Color(0xFF747372),
  'detailsColor2': Color(0xFF2a3648)
};