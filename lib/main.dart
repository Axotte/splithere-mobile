import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:splithere/injection/app_module.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]);
  runApp(AppModule());
}
