import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:splithere/screens/login/widgets/form_field_label.dart';
import 'package:splithere/screens/login/widgets/no_account_button.dart';
import 'package:splithere/widgets/expandable_container.dart';
import 'package:splithere/widgets/single_info_row.dart';

void main() {
  group('Golden image tests', () {
    testWidgets('Single row widget test', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: SingleRow(
          label: 'test label',
          text: 'test text',
          textColor: Colors.amber,
          labelColor: Colors.pink,
        ),
      ));
      await expectLater(find.byType(SingleRow), matchesGoldenFile('single_row.png'));
    });

    testWidgets('Expandable container test', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: ExpandableContainer(
          collapsedHeight: 100,
          expandedHeight: 300,
          expanded: true,
          child: Container(
            width: 20,
            height: 20,
            color: Colors.pink,
            child: Text('lzemonzcgidZ a PaainrzinabtmeaZz susa'),
          ),
        ),
      ));
      await expectLater(find.byType(ExpandableContainer), matchesGoldenFile('expandable_container.png'));
    });

    testWidgets('form field label test', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: FormFieldLabel(
          text: 'ćivk',
        ),
      ));
      await expectLater(find.byType(FormFieldLabel), matchesGoldenFile('form_field_label.png'));
    });

    testWidgets('no account button test', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: NoAccountButton(
          text: 'Wkąot Jkłtk',
          onPressed: () {},
        ),
      ));
      await expectLater(find.byType(NoAccountButton), matchesGoldenFile('no_account_button.png'));
    });
  });
}