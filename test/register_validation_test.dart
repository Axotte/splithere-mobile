import 'package:bloc_pattern/bloc_pattern_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:splithere/blocs/register_validation_bloc.dart';
import 'package:splithere/config/app_config.dart';
import 'package:splithere/enums/grant_type.dart';
import 'package:splithere/injection/login_module.dart';
import 'package:splithere/models/token/token_request.dart';

void main() {
  group('register validation tests', () {
    test('all empty fields', () async {
      initModule(LoginModule());
      
      RegisterValidationBloc registerBloc = LoginModule.injector.getBloc();
      registerBloc.validateAndGetRequest();

      await expectLater(registerBloc.passwordStream, emitsError('empty_field_error'));
      await expectLater(registerBloc.repeatPasswordStream, emitsError('empty_field_error'));
      await expectLater(registerBloc.loginStream, emitsError('empty_field_error'));
      await expectLater(registerBloc.emailStream, emitsError('empty_field_error'));
    });

    test('only email empty', () async {
      initModule(LoginModule());

      RegisterValidationBloc registerBloc = LoginModule.injector.getBloc();
      registerBloc.changePassword('Zaq12wsx');
      registerBloc.changeRepeatPassword('Zaq12wsx');
      registerBloc.changeLogin('Test123');
      registerBloc.validateAndGetRequest();

      await expectLater(registerBloc.passwordStream, emits('Zaq12wsx'));
      await expectLater(registerBloc.repeatPasswordStream, emits('Zaq12wsx'));
      await expectLater(registerBloc.loginStream, emits('Test123'));
      await expectLater(registerBloc.emailStream, emitsError('empty_field_error'));
    });

    test('Password to short', () async {
      initModule(LoginModule());

      RegisterValidationBloc registerBloc = LoginModule.injector.getBloc();
      registerBloc.changePassword('Zaq1');
      registerBloc.changeRepeatPassword('Zaq1');
      registerBloc.changeLogin('Test');
      registerBloc.changeEmail('test@test.com');
      registerBloc.validateAndGetRequest();

      await expectLater(registerBloc.passwordStream, emitsError('password_to_short_error'));
      await expectLater(registerBloc.repeatPasswordStream, emitsError('password_to_short_error'));
    });

    test('Password not matches', () async {
      initModule(LoginModule());

      RegisterValidationBloc registerBloc = LoginModule.injector.getBloc();
      registerBloc.changePassword('Zaq12wsx');
      registerBloc.changeRepeatPassword('Zaq12wsxxxxxxxxx');
      registerBloc.changeLogin('Test');
      registerBloc.changeEmail('test@test.com');
      registerBloc.validateAndGetRequest();

      await expectLater(registerBloc.passwordStream, emitsError('different_password_error'));
      await expectLater(registerBloc.repeatPasswordStream, emitsError('different_password_error'));
    });

    test('Null on invalid data', () async {
      initModule(LoginModule());

      RegisterValidationBloc registerBloc = LoginModule.injector.getBloc();

      final result = registerBloc.validateAndGetRequest();

      expect(result, null);
    });

    test('Valid data', () async {
      initModule(LoginModule());

      RegisterValidationBloc registerBloc = LoginModule.injector.getBloc();
      registerBloc.changePassword('Zaq12wsx');
      registerBloc.changeRepeatPassword('Zaq12wsx');
      registerBloc.changeLogin('Test123');
      registerBloc.changeEmail('test@test.com');
      final result = registerBloc.validateAndGetRequest();

      expect(result, isInstanceOf<TokenRequest>());
      expect(result.email, 'test@test.com');
      expect(result.password, 'Zaq12wsx');
      expect(result.confirmPassword, 'Zaq12wsx');
      expect(result.nickname, 'Test123');
      expect(result.grantType, GrantType.PASSWORD);
      expect(result.clientId, AppConfig.clientId);
      expect(result.clientSecret, AppConfig.clientSecret);
    });
  });

}